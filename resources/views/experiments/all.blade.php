{{Analytics::trackEvent('Página', 'Dashboard')}}

@extends ('layout.default')

@section('page')
{{trans('interface.name', ['page'=>trans('experiments.title')])}}
@stop
<style>
    .panel{
        width: 95%;
        border:none;
        padding:10px;
    }
    .panel-body{
        font-size:12pt;
        background: #ECF0F1;
        min-height: 180px;
        margin:0;
        border:none;
        border-radius: 0 5px;

    }
    .panel-body > .row >  div{
        padding: 0 8px 8px 8px;
    }
    .panel-body > .row >  div > img{
        width: 100%;
    }
    .panel-body > .row >  div > b{
        font-size: 12pt;
    }   
    .panel-body > .row >  div > p{
        font-size: 11pt;
    }

    @media only screen and (min-device-width: 1024px){
        .panel-body > .row >  div > img{
            height: 140px;
            min-width: 80%;
            max-width: 100%;
        }
        .panel-body > .row >  div > b{
            font-size: 16pt;
        }
        .panel-body > .row >  div > p{
            font-size: 14pt;
        }
    }

    @media only screen and (device-width: 601px) 
    and (device-height: 906px) 
    and (-webkit-min-device-pixel-ratio: 1.331) 
    and (-webkit-max-device-pixel-ratio: 1.332){
        .panel-body > .row >  div > b{
            font-size: 16pt;
        }
        .panel-body > .row >  div > p{
            font-size: 14pt;
        }
    }

</style>

@section('title')
{{trans('experiments.title')}}
@stop

<?php
$name = 'name_' . App::getLocale();
$desc = 'description_' . App::getLocale();
?>

@section ('content')

<div class="row" style="padding-bottom:20px">
    @foreach($exps as $exp)
        <div class="col-lg-6 col-md-6 col-sm-6  lab">
            <div class="panel" style="background: #ECF0F1; ">
                <div class="panel-body">
                    <div class="row">
                        <div class='col-lg-4 col-md-4 col-sm-3 col-xs-12'>
                            <img src="{{asset('/img/experiments/'.$exp->img)}}">
                        </div>
                        <div class="col-lg-7 col-md-7 col-sm-8 col-xs-12">
                            <b style="line-height:1">{{$exp->$name}}</b><br>
                            <a href=""><i class="fa fa-flask" aria-hidden="true"></i> {{$exp->lab_name}}</a>
                            <p style="font-size:11pt; line-height: 1.2; padding-top:5px;"><?php echo $exp->$desc; ?></p>
                            <a href="{{url('/labs/'.$exp->lab_id.'/exp/'.$exp->id)}}" class="btn-primary btn-sm" role="button">{{trans('interface.access')}}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
  
    @endforeach

</div>
@stop

