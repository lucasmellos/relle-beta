<?php

namespace App\Http\Middleware;

use Closure;

class Cors {
    public function handle($request, Closure $next)
    {
        return $next($request)
            ->header('Access-Control-Allow-Origin', '*')
            ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
	    ->header('X-Frame-Options', 'ALLOW FROM https://www.lte.ib.unicamp.br/portal/experiments.php?idExperiment=1&embed=true');
    }
}

