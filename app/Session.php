<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Session extends Model {

    protected $fillable = [
        'id',
        'payload',
        'last_activity',
        'lang',
    ];

}
