<?php
namespace App\Http\Controllers;
use App\Experiments;
use App\Labs;
use App\Instances;
use App;
use Route;
use Input;
use File;
use Request;
use Image;
use Auth;

class ExperimentsController extends Controller {
    
    public function all() {
        $exps = Experiments::all();
        $name = 'name_' . App::getLocale();
        foreach ($exps as $one){
            $one->lab_name = Labs::find($one->lab_id)->$name;
        }
        return view('experiments.all', compact('exps'));
    }
    
    function one() {
        
        if (Auth::guest()) {
            return redirect()->guest('login');
        }
        
        $id = Route::getCurrentRoute()->parameters()['expid'];
        $exp = Experiments::find($id);
        $exp->lang = App::getLocale();
        $exp->lab= Labs::find($exp->lab_id);
        
        $inst = Instances::where('lab_id', "=",$exp->lab_id)->get();
        
        $path = public_path().'/exp_data/'.$exp->lab_id.'/exp/'.$exp->id.'/questions.json';
        $qst = json_decode(File::get($path), true);    
        
        foreach($qst as &$one){
            unset($one['answear']);
        }
        
        
        $questions = json_encode($qst);
        
        
        
        return view('experiments.one', ['exp'=>$exp, 'inst'=>$inst, 'questions'=>$questions]);
    }
    
    function create() {
        $labs = Labs::all();
                
        return view('experiments.create', ['labs'=>$labs]);
    }
    
    function save(){
        $exp = new Experiments;        
        $input = Request::all();
        
        //print_r($one);die;
        
        $exp->name_pt = $input['name_pt'];
        $exp->description_pt = $input['description_pt'];
        $exp->tags = $input['tags'];
        $exp->lab_id = $input['labid'];
        
        $exp->save();

        $tmp_path = $input['img']->getRealPath();
        $extension = $input['img']->getClientOriginalExtension();
        $name = uniqid() . '.' . $extension;
        $path = '/img/experiments/' . $name;
        Image::make($tmp_path)->save(base_path() . '/public' . $path);
        $exp->img = $name;
        
        File::makeDirectory(base_path() .'/public/exp_data/'.$input['labid'].'/exp/'.$exp->id, 0775, true);
        
        $path = base_path() . '/public/exp_data/'.$input['labid'].'/exp/'.$exp->id.'/questions.json';
        File::put($path, json_encode(json_decode($input['questions'])));
        $exp->save();
    }
    
    
    function verify(){
        $input = Request::all();
        
        $path = public_path().'/exp_data/'.$input['lab'].'/exp/'.$input['exp'].'/questions.json';
        $questions = json_decode(File::get($path), true);
        
        if(is_array($input['answer'])){
            return ($questions[$input['question']]['correctResponsesPattern'] === $input['answer']) ? 'true' : 'false';
        }

        
        if($input['answer']==$questions[$input['question']]['correctResponsesPattern'][0]){
            return 'true';
        }else{
            return 'false';
        }
    }
    
}