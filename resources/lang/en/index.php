<?php

return [
    'title' => 'Home',
    'read' => 'Documentation',
    'access' => 'Access the Labs',
    'description' => 'Remote experimentation allows the remote access to real labs using the internet.',
    'exp'=> 'The experiments can be accessed anytime, from anywhere.',
    
    'what' => 'What is a remote lab?',
    'what-desc' => 'You can use real equipment to experiment through the internet, anytime and from anywhere.',
    'create' => 'Create your own labs',
    'create-desc' => 'Besides accessing the remote labs available, you can use RELLE to create your own remote labs.',
    'courses' => 'Courses',
    'courses-desc' => 'You can also access courses that integrate the use of Remote Experiments during the activities development!',
    'all-courses' => 'All courses',
];

