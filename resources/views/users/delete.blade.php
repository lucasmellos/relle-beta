@extends ('layout.dashboard')

@section('page')
{{trans('interface.name', ['page'=>trans('users.delete')])}}
@stop


@section ('title_inside')
{{trans('users.delete')}}
@stop
    {{Analytics::trackEvent('Páginas', 'Deletar Usuário')}}

@section ('inside')
{!!
Form::open([
'action' => 'UsersController@doDelete',
])
!!}
<input type="hidden" name='id' value="{{$id}}"/>

<ul class="list-group panel-default">
    <li class="list-group-item" style="text-align: center"><strong>{{trans('message.action')}}</strong></li>
    <li class="list-group-item" style="text-align: center">{{trans('message.delete_user')}}</li>
</ul>
<a href="{{url('dashboard')}}" role="button"  class="btn btn-success">{{trans('interface.cancel')}}</a>
<input class="btn btn-danger" type="submit" value="{{trans('interface.delete')}}">
{!!
Form::close()
!!}
@stop
