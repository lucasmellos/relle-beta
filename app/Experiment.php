<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Experiment extends Model {

    protected $fillable = [
        'id',
        'lab_id',
        'name_pt',
        'description_pt',
        'name_en',
        'description_en',
        'name_es',
        'description_es',
        'tags',
        'img',
        'created_at',
        'updated_at'
    ];

}