<?php

namespace App\Http\Controllers\Auth;

use Auth;
use App\User;
use Socialite;
use App\Http\Controllers\Controller;
use Illuminate\Support\MessageBag;
use Input;
use Mail;
use Session;
use Response;
use Redirect;
use DB;
use Route;
use App;


class AuthController extends Controller
{
    // Some methods which were generated with the app
    /**
     * Redirect the user to the OAuth Provider.
     *
     * @return Response
     */
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from provider.  Check if the user already exists in our
     * database by looking up their provider_id in the database.
     * If the user exists, log them in. Otherwise, create a new user then log them in. After that 
     * redirect them to the authenticated users homepage.
     *
     * @return Response
     */
    public function handleProviderCallback($provider)
    {
        $user = Socialite::driver($provider)->user();

        $authUser = $this->findOrCreateUser($user, $provider);
        Auth::login($authUser, true);
        return redirect('');
    }

    /**
     * If a user has registered before using social auth, return the user
     * else, create a new user object.
     * @param  $user Socialite user object
     * @param $provider Social auth provider
     * @return  User
     */
    public function findOrCreateUser($user, $provider)
    {
        $authUser = User::where('provider_id', $user->id)->first();
        if ($authUser) {
            return $authUser;
        }
        return User::create([
            'name'     => $user->name,
            'email'    => $user->email,
            'provider' => $provider,
            'provider_id' => $user->id
        ]);
    }
    public function login() {
        
        $userdata = array(
            'username' => Input::get('username'),
            'password' => Input::get('password')
        ); 
        //dd(Auth::attempt($userdata));
        if (Auth::attempt($userdata)){
            return redirect('');
		}else{
            
            session(['error' => trans('message.login')]);
             
            return redirect()->back()->withInput();
		}
	}
    public function logout() {
        Auth::logout(); // log the user out of our application
        //keep language preferences
        $lang = session('lang');
        Session::flush();
        Session::put('lang', $lang);
        //logout moodle
        //return redirect('http://relle.ufsc.br/moodle/login/auth_logout.php'); 
        return view('login.logout'); 
    }
    public function forgot() {
        return view('login.forgot');
    }
    public function reset() {
        $token = Route::getCurrentRoute()->parameters();
        return view('login.reset')->with($token);
    }
    public function sendPass() {
        $input = Input::all();
        $user = User::where('email', '=', $input['email'])->first();
        $errors = new MessageBag;
        if ($user) {
            try {
                $name=explode(' ',$user->name);
                $firstname=$name[0];
                $lastname=end($name);
                $user = $user->toArray();
                $data ['username'] = $user['name'];
                $data['token'] = $input['_token'];
                $data['firstname'] = $firstname;
                $data['lastname'] = $lastname;
                $data['email'] = $user['email'];
                Session::put('data', $data);
                Mail::send('mail.reset_' . App::getLocale(), $data, function($message) {
                    $message->to(session('data')['email'], session('data')['firstname'], session('data')['lastname'])->subject(trans('login.forgot'));
                });
            } catch (Exception $e) {
                $errors->add('reset', trans('message.trans'));
                return redirect()->back()->withErrors($errors);
            }
            Session::pull('data');
            return redirect('login')->withErrors(['sent' => trans('message.sent')]);
        } else {
            Session::pull('data');
            $errors->add('email', trans('message.email'));
            return redirect()->back()->withErrors($errors);
        }
    }
}