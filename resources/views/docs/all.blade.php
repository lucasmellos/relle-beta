@extends ('layout.dashboard')

@section('page')
{{trans('interface.name', ['page'=>trans('docs.title_page')])}}
@stop

@section('title_inside')
{{trans('docs.title_page')}}
@stop

<!--{{$name='name_'.App::getLocale()}}
{{$desc='description_'.App::getLocale()}}-->


@section ('inside')
<div class="table-responsive">
    <table class="table">
        <tr>
            <td></td>
            <td>{{trans('docs.title')}}</td>
            <td>{{trans('docs.type')}}</td>
            <td>{{trans('docs.size')}}</td>
            <td>{{trans('docs.modified')}}</td>
            <td></td>
        </tr>
        
        @foreach($docs as $doc)

        <tr>
            <td><img src="{{asset('img/docs/'.$doc['format'].'.png')}}" height="30px"></td>
            <td>{{$doc['title']}}</td>
            <td>{{trans('docs.'.$doc['type'])}}</td>
            <td>{{$doc['size']}}</td>
            <td>{{date('d/m/Y H:i', strtotime($doc['created_at']))}}</td>
            <td>
                <a href="/docs/{{$doc->id}}/delete"><span class="glyphicon glyphicon-trash text-danger" data-toggle="tooltip" title="Excluir"/></a>
                <a href="/docs/{{$doc->id}}/edit"><span class="glyphicon glyphicon-pencil text-success" data-toggle="tooltip" title="Editar"/></a>
                <a href="{{asset($doc['url'])}}"><span class="glyphicon glyphicon-save" data-toggle="tooltip" title="Download"/></a>
            </td>
        </tr>
        @endforeach
        
    </table>
</div>
@stop


