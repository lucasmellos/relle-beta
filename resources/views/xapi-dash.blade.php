@extends ('layout.default')

@section('page')
xAPI Dashboard
@stop



@section('script')
<script src="{{asset('js/xapi/xapidashboard.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/xapi/xapicollection.min.js')}}" type="text/javascript"></script>
<script>
    $(function(){
        
        var wrapper = ADL.XAPIWrapper;
        wrapper.changeConfig({"endpoint" : 'https://lrs.adlnet.gov/xAPI/'});
        var dash = new ADL.XAPIDashboard();

        window.onload = function(){
                // get all statements made in the last two weeks
                var query = {'since': new Date(Date.now() - 1000*60*60*24*30).toISOString()};
                dash.fetchAllStatements(query, fetchDoneCallback);
        };
        
        function fetchDoneCallback(){
	var chart = dash.createBarChart({
		container: '#graphContainer svg',
		groupBy: 'verb.id',
		aggregate: ADL.count(),
		customize: function(chart){
			chart.xAxis.rotateLabels(45);
			chart.xAxis.tickFormat(function(d){ return /[^\/]+$/.exec(d)[0]; });
		}
	});
	chart.draw();
}
});
chart.draw();
    });
</script>
@stop


