@extends ('layout.dashboard')

@section('page')
{{trans('interface.name', ['page'=>trans('book.create')])}}
{{$name='name_'.App::getLocale()}}
@stop

@section ('title_inside')
{{trans('book.create')}}

<link href="{{asset('css/datepicker.css')}}" rel="stylesheet">
<link href="{{asset('css/bookingCreate.css')}}" rel="stylesheet">
<?php $horaServidor = date('H:i:s') ?>

@stop

@section ('inside')

<!------------------------------Mensagens de Erro--------------------------------->
<div id='error1' style='display:none' class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>{{ trans('book.error1') }}</strong>&nbsp{{ trans('book.durac2') }}
</div>
<div id='error2' style='display:none' class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>{{ trans('book.error2') }}&nbsp</strong> 
</div>
<div id='error3' style='display:none' class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>{{ trans('book.error3') }}</strong>
</div>
<div id='error4' style='display:none' class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>{{trans('book.sorry')}},</strong>&nbsp{{ trans('book.error4') }} 
</div>
<div id='error5' style='display:none' class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>{{trans('book.please')}},</strong>&nbsp{{ trans('book.error5') }}
</div>
<!-------------------------------------------------------------------------------->

<!-----------------------Inicio da Estrutura da página---------------------------->
<div class="row">
    <div class="form-group col-lg-4 col-md-6 col-sm-12" id="linha1">
        <label>{{trans('book.exp')}}</label>
        <select class="form-control select select-default" name="type" id="exp_id" onchange="mudarDescricao()">
            @foreach($labs as $lab)
                <option data-duration="{{$lab->duration}}" value="{{$lab->id}}">{{$lab->$name}}</option>
            @endforeach
        </select>
        <small>{{trans('book.small1')}}<span id="small1">8</span>{{trans('book.min')}}</small>
    </div>
    <div class="form-group col-lg-4 col-sm-12" id="linha1">
        <label>{{trans('book.duration')}}</label>
        <select class="form-control select select-default" id="duration2">
            <option  value="nse">{{trans("book.nse")}}</option>
            <option  value="nmin" selected="selected">{{trans("book.nmin")}}</option>
        </select>
    </div>
    <div class="form-group col-lg-2 col-sm-12" id="linha1">
        <input type=number min="1" max="120" class="form-control" id="durationBox" value="10" style="margin:39px 0px; text-align: left">
    </div>
</div>

<div class="row" >
    <div class="form-group col-lg-4 col-sm-12">
        <label>{{trans('book.date')}}</label>
        <input type="text" value="<?php echo date("Y-m-d"); ?>" id="datepickerID" class="form-control">
    </div>
    <div class="form-group col-lg-4 col-sm-12">
        <label>{{trans('book.time')}}</label>
        <input type="time" class="form-control" value="<?php echo date("H:i"); ?>" id="timepickerID">
        <small>{{trans('book.small2')}}</small>
    </div>
</div>

<div class="row">
    <div class="form-group col-md-12">
        <button onclick="calcDuration();submitOutput()" class="btn btn-primary btn-wide">{{trans("interface.create")}}</button>
    </div>
</div>
<div id="horaServ">
    <span id='hour'></span>:<span id='min'></span>:<span id='seg'></span>
    <label style="font-size:10px;font-weight: bold; color: #2C3E50">&nbsp (GMT - 03:00 / Brasilia, Brasil) </label>
</div>
<!-------------------------------------------------------------------------------->
@stop

@section ('script_dash')
<script src="{{asset('js/BookingCreate.js')}}" rel="text/javacript"></script>
<script src="{{asset('js/datepicker.js')}}"></script>

<script>
    var serverHour = "<?php echo $horaServidor; ?>";
    var serverHourArray = serverHour.split(':');

    $("#hour").html(serverHourArray[0]);
    $("#min").html(serverHourArray[1]);
    $("#seg").html(serverHourArray[2]);

    function startTime() {  
        var h = today.getHours();
        var m = today.getMinutes();
        var s = today.getSeconds();
        m = checkTime(m);
        s = checkTime(s);
        document.getElementById('clock').innerHTML =
        h + ":" + m + ":" + s;

    }

    function clock1() {
        var totalseg = 1+ parseInt($("#hour").html())*60*60 + parseInt($("#min").html()) * 60 + parseInt($("#seg").html());
        var mins = totalseg / 60;
        var horas = parseInt(mins/60);
        var seg = Math.round(totalseg - parseInt(mins)*60);
        var min = parseInt(mins-parseInt(horas)*60);
        
        zero = "";
        if (horas < 10) {
            zero = "0";
        }
        $("#hour").html(zero + horas);

        var zero = "";
        if (seg < 10) {
            zero = "0";
        }
        $("#seg").html(zero + seg);
        
        zero = "";
        if (min < 10) {
            zero = "0";
        }
        $("#min").html(zero + min);

    }

    $(document).ready(function () {
        setInterval(clock1, 1000);
    });
</script>
@stop