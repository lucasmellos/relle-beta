@extends ('layout.dashboard')

@section('page')
{{trans('interface.name', ['page'=>trans('book.title_remove')])}}
{{$lang = "name_". App::getLocale() }}
@stop

@section('title_inside')
{{trans('book.title_remove')}}
@stop

@section ('inside')

<input type="hidden" name='id' value="{{$booking->id}}"/>

<ul class="list-group panel-default">
    <li class="list-group-item"><i class="fa fa-calendar-o">&nbsp;&nbsp;</i>{{trans('book.date')}}: {{$booking->date}}</li>
    <li class="list-group-item"><i class="fa fa-bell-o">&nbsp;&nbsp;</i>{{trans('book.time')}}: {{$booking->time}}</li>
    <li class="list-group-item"><i class="fa fa-minus">&nbsp;&nbsp;</i>{{trans('book.token')}}: <strong>{{$booking->token}}</strong></li>
    <li class="list-group-item"><i class="fa fa-flask">&nbsp;&nbsp;</i>{{trans('book.exp')}}: <strong>{{$exp->name_pt}}</strong></li>
</ul>

<form class="form-vertical" method="post" action="{{url('booking/'.$booking->token.'/delete')}}">
    <input type="hidden" name='id' value="{{$booking->id}}"/>
    <input type="hidden" name='_token' value="{{csrf_token()}}"/>
    <input class="btn btn-danger pull-right" type="submit" value="{{trans('interface.delete')}}">
</form>
<a href="{{url('dashboard')}}" role="button"  class="btn btn-success">{{trans('interface.cancel')}}</a>
@stop
