<?php

namespace App\Http\Controllers;

use App;
use App\User;
use Excel;
use ExcelFile;
use Input;
use DB;
use PDO;
use Config;

class UsersImportController extends Controller {

    public function show() {
        return view('users.import');
    }

    public function import() {

        $form = Input::all();
        $file = $form['file']->getRealPath();
        Config::set('Excel::csv.delimiter', ';');
        Config::set('Excel::csv.lineEnding', '\n');
        Excel::load($file, function($reader) {

            $user = new User();

            $results = $reader->get()->toArray();

            foreach ($results as $input) {
                if (!empty($input['username'])) {
                    unset($input['0']);

                    User::create($input);

                }
            }
        });
        User::where('username', '=', '')->delete();
        return redirect('/users');
    }

    function export() {

        Excel::create('Users', function($excel) {

            $excel->sheet('Users', function($sheet) {

                $users = User::select('username', 'email', 'name', 'password', 'organization', 'type')->get();
                $sheet->fromArray($users);
            });
        })->export('csv');
    }

    function export_bulk() {

        Excel::create('Users', function($excel) {
            $excel->sheet('Users', function($sheet) {
                
                $input = Input::all();
                $users = explode(',', $input['users']);

                DB::setFetchMode(PDO::FETCH_ASSOC);
                $query = 'SELECT username, email, firstname, lastname ,password, organization,country, type FROM users WHERE';
                $n = count($users);
                if ($n == 1) {
                    $query.=' username = "' . $users[$i] . '"';
                } else {
                    for ($i = 0; $i < $n; $i++) {
                        if ($i < $n - 1) {
                            $query.=' username = "' . $users[$i] . '" OR';
                        } else {
                            $query.=' username = "' . $users[$i] . '"';
                        }
                    }
                }

                $data = DB::select($query);
                $sheet->fromArray($data);
            });
        })->download('csv');
    }

}
