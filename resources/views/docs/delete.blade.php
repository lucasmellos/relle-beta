@extends ('layout.dashboard')

@section('page')
{{trans('interface.name', ['page'=>trans('docs.delete')])}}
@stop

@section('title_inside')
{{trans('docs.delete')}}
@stop

@section ('inside')
<?php
$title = $doc->{'title'};
$tag = $doc->{'tags'};
?>
<style>
    .btn-file{
        border-radius: 5px;
        margin:3px;
    }
    .bootstrap-tagsinput{
        border: 2px solid #bdc3c7;
    }
    .bootstrap-tagsinput:focus{
        border: 2px solid #1abc9c;
    }
</style>

{!!
Form::open([
'files' => true,
'enctype'=> 'multipart/form-data',
'class' => 'dropzone'
])
!!}

<ul class="list-group panel-default">
    <li class="list-group-item">{{trans('docs.title')}}: <strong>{{$title}}</strong></li>        
    <li class="list-group-item">{{trans('docs.tag')}}: {{$tag}}</li>
</ul>
<a href="{{url('dashboard')}}" role="button"  class="btn btn-success">{{trans('interface.cancel')}}</a>
<input class="btn btn-danger" type="submit" value="{{trans('interface.delete')}}">

{!!Form::close()!!}
@stop
