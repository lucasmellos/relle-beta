@extends ('layout.dashboard')

@section('page')
{{trans('interface.name', ['page'=>trans('book.title_page')])}}
{{$lang = "name_". App::getLocale() }}
{{$user = 'admin'}}
{{!!Analytics::trackPage('Página', 'Bookings')}}
@stop

@section('title_inside')
{{trans('book.title_page')}}
@stop

@section ('inside')
<div class="alert alert-success" id="alert" style="display: none">
    <strong>Sucesso!</strong> Token copiado!
</div>

<div class="table-responsive">
    <table class="table table-responsive table-striped">
        <tr>
            <td></td>
            @if(Auth::user()->type == 'admin')
            <td><strong>{{trans('book.user')}} </strong></td>
            @endif
            <td><strong>{{trans('book.date')}} </strong></td>
            <td><strong>{{trans('book.time')}} </strong></td>
            <td><strong>{{trans('book.durac')}}</strong></td>
            <td><strong>{{trans('book.exp')}}  </strong></td>
            <td><strong>{{trans('book.token')}}</strong></td>
            <td></td>
        </tr>
        <?php $n = 0?>
        @foreach ($bookings as $booking)
            @if ($booking->created_by == Auth::user()->id || Auth::user()->type == 'admin' )
            <tr> 
                <td><?php ++$n ?></td>
                @if(Auth::user()->type == 'admin')
                <td>{{$booking->name}}</td>
                @endif
                <td>{{$booking->date}}</td>
                <td>{{$booking->time}}</td>
                <td>{{$booking->duration}} min</td>
                <td>{{$booking->$lang}}</td>
                <td style="font-size: 15px"><a id="token" data-clipboard-text="{{$booking->token}}" href="#" style="font-weight: 600; color: rgb(52, 73, 94)" data-toggle="tooltip" title="Copiar">{{$booking->token}}</a></td>
                <td>
                    <a href="{{$booking->token}}/edit"><span class="glyphicon glyphicon-pencil text-success" data-toggle="tooltip" title="Editar"/></a>
                    <a href="{{$booking->token}}/delete"> <span class="glyphicon glyphicon-trash text-danger" data-toggle="tooltip" title="Remover" value="Reload Page" /></a>
                </td>
            </tr>
            @endif
        @endforeach 
    </table>
  
    @if($n == 0)
    <style>
       .block {
        height: 151px;
        width: auto;
       }
       #text2{
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        opacity: 0.5;
        }
        td{
            display:none;
        }
        </style>
        <div class="block"><a href="http://beta.relle.ufsc.br/teste/booking/create"></a></div>
        <h1 id="text2" style="font-size: 15px;">Não existem Agendamentos :(</h1>     
    @endif
</div>
<script src="{{asset('js/clipboard.min.js')}}"></script>
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
var btn = document.getElementById('token');
var clipboard = new Clipboard(btn);

    clipboard.on('success', function(e) {
        console.log(e);
    });

    clipboard.on('error', function(e) {
        console.log(e);
    });
  
$("#token").click(function(){
    $("#alert").css("display", "block");
    setTimeout(function(){ $("#alert").css("display", "none"); },2000);
});
</script>
@stop
