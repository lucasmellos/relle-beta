<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Request;

class User extends Authenticatable
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'email', 'password' ,'type', 'organization', 'provider', 'provider_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public function getUser(Request $request){
        if (Auth::user()){
            return Auth::user(); 
       }else{
           return false;
       }
       
   }
}
