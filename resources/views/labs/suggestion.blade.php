        
            <!--Suggestions -->
            <h5 style="font-weight:normal; padding-top:50px">{{trans('labs.similar')}}</h5>
            <div class="row">
            
            @foreach($suggestions as $lab)
                <div class="col-lg-6 col-md-6 col-sm-6">
                
                    <div class="panel panel-default " style="background: #ECF0F1;height: 20rem; ">
                        <div class="panel-body sug">
                            <div class="row">
                                <div class='col-lg-4 col-md-4 col-sm-4 col-xs-4'>
                                    <img class="fotos_temporario" src="{{asset($lab->thumbnail)}}" width="100%">
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                    <b style="line-height: 1.50">{{$lab->$name}}</b>
                                    <p style="font-size:11pt; line-height: 1.2; padding-top:5px;"><?php echo $lab->$desc; ?></p>
                                    <a href="{{url('/labs/'.$lab->id)}}" class="btn btn-xs btn-primary" role="button">{{trans('interface.access')}}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
                
                @endforeach

            </div>

            <!-- Facebook Comments -->
            <h5 style="font-weight:normal; padding-top:50px">{{trans('interface.comments')}}</h5>
            <div class="fb-comments" data-width="100%" data-numposts="10"></div>

