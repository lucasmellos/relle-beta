<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLabsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('labs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_pt');
            $table->string('name_en');
            $table->string('name_es');
            $table->string('description_pt');
            $table->string('description_en');
            $table->string('description_es');
            $table->string('tags');
            $table->string('duration');
            $table->string('target');
            $table->string('subject');
            $table->string('difficulty');
            $table->string('interaction');
            $table->string('thumbnail');
            $table->string('tutorial_pt')->nullable();
            $table->string('tutorial_en')->nullable();
            $table->string('tutorial_es')->nullable();
            $table->string('video')->nullable();
            $table->integer('queue');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('labs');
    }
}
