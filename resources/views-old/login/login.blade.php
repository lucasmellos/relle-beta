{{Analytics::trackEvent('Página', 'Login')}}
@extends ('layout.default')

@section('page')
{{trans('interface.name', ['page'=>trans('login.login')])}}
@stop

@section('head')
<!--<style>

    .profile-img {
        width: 96px;
        height: 96px;
        margin: 0 auto 10px;
        display: block;
        -moz-border-radius: 50%;
        -webkit-border-radius: 50%;
        border-radius: 50%;
    }
    #tabs{
        margin-top: -10px;
    }

</style> -->
@stop

@section ('content')
@if(session('error'))   
<div class="alert alert-danger">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>Erro:</strong> {{session('error')}}.
</div>
<?php session()->forget('error'); ?>
@endif
<center>
    <div class="row">

        <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">

            <div id="my-tab-content" class="tab-content login_form" >
                <img class="profile-img login_form-img" src="{{ asset('/img/default.gif') }}" alt=""> 

                <form class="form-group form-signin" id='form' method="POST">

                    <center><h1>{{trans('login.login')}}</h1></center>
                    <input name="username" id='username' type="text" class="form-control login_form-input-1" placeholder="{{trans('login.username')}}" required autofocus>
                    <input name="password" id='password' type="password" class="form-control login_form-input-2 " placeholder="{{trans('login.password')}}" required>
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <button id="submit" class="btn btn-lg btn-fresh btn-block login_form-btn">{{trans('interface.submit')}}</button>

                    <a href="{{ url('/auth/facebook') }}" class="btn btn-facebook login_form-btn--social"><i class="fa fa-facebook"></i> Facebook</a>
                    <a href="{{ url('/auth/google') }}" class="btn btn-google login_form-btn--social"><i class="fa fa-google"></i> Google</a>
                    <a href="{{ url('/auth/github') }}" class="btn btn-github login_form-btn--social"><i class="fa fa-github"></i> Github</a>
                    </div>
                </form>

                <div id="tabs" data-tabs="tabs" class="login_form-tabs">
                    <a href="{{url('/users/signup')}}">{{trans('login.signup_msg')}} </a>
                    <a href="{{url('/login/forgot')}}">{{trans('login.forgot_msg')}}</a>
                </div> 

            </div>  
        </div>      
    </div>
</center>
@stop

@section('script')
<script>
    //default goto
   /*
    $(function () { 
                var goto = '{{url("/")}}';
                var login_url = '{{url("login")}}';
      $('#submit').click(function (event) {
          
          
            var formData = {             
                    'username': $('input[name=username]').val(),
                    'password': $('input[name=password]').val(),
                    '_token': $('input[name=_token]').val()
            };
            $.ajax({
                    method:'post',
                    url: login_url,
                    data: formData,
                    success: function (data) {    
                        console.log(data);
                        if (data.goto) {
                            goto = data.goto;
                            window.location.href=goto;
                            console.log('primeiro');
                        } else if (data.errors) {
                            $("#error").show(1000);
                            console.log(data.errors);
                            console.log('segundo');
                            event.preventDefault();
                        }else{
                            window.location.href=goto;
                            console.log('terceiro');
                            event.preventDefault();
                        }
                    },
                    error: function(){
                        $("#error").show(1000);                       
                    }
            });
            });
        });
        */
</script>
@stop

