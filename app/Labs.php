<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Labs extends Model {

    public $timestamps = false;
    protected $fillable = [
        'id',
        'name_pt',
        'name_en',
        'name_es',
        'description_pt',
        'description_en',
        'description_es',
        'tags',
        'duration',
        'target',
        'subject',
        'difficulty',
        'interaction',
        'thumbnail',
        'maintenance',
        'queue',
        'tutorial_pt',
        'tutorial_en',
        'video'
    ];

}
