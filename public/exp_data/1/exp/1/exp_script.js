
//xapi Functions
function sendStatement(data) {       
    
    var objTypes = {
        "laboratory": "http://example.com/relle/onlineLabs/laboratory",
        "sensor": "http://example.com/relle/onlineLabs/sensor",
        "actuator": "http://example.com/relle/onlineLabs/actuator",
        "tutorial": "http://example.com/relle/onlineLabs/tutorial",
        "question" : "http://adlnet.gov/expapi/activities/question"
    };

    try {
        lrs = new TinCan.LRS(
                {
                    endpoint: "http://learninglocker.rexlab.ufsc.br/data/xAPI/",
                    username: "bb1740e8ab66a5cdabbcb76e547448534d355338",
                    password: "4d0c90c03ad247eee5b76b982bdf1434c5198e3f",
                    allowFail: false
                }
        );
    } catch (ex) {
        console.log("Failed to setup LRS object: " + ex);
        // TODO: do something with error, can't communicate with LRS
    }
    //wait for sensors update on main object
    //console.log("waiting xapi...");
    var obj;
    var stmt;
    var object_data = ('definition' in data.obj) ? data.obj : getObject(data.obj.id, data.obj.name, objTypes[data.obj.type]);
    object_data.objectType = "Activity";
   
    obj = {
        "actor": data.actor,
        "verb":getVerb(data.verb),
        "object": object_data
    };
    
    
    if(("context" in data)){ 
        obj["context"] = getContext(data.context);
    }
    if(("result" in data)){ 
        
        obj["result"] = ('success' in data.result) ? data.result : getResult(data.result);
    }
    stmt = new TinCan.Statement(obj);
        
    
    
    console.log(stmt);


    lrs.saveStatement(stmt,
            {
                callback: function (err, xhr) {
                    if (err !== null) {
                        if (xhr !== null) {
                            console.log("Failed to save statement: " + xhr.responseText + " (" + xhr.status + ")");
                            // TODO: do something with error, didn't save statement
                            return;
                        }
                        console.log("Failed to save statement: " + err);
                        // TODO: do something with error, didn't save statement
                        return;
                    }
                    console.log("Statement saved");
                    // TOOO: do something with success (possibly ignore)
                }
            }
    );
        
}

function getVerb(verb){
    var verbs = {
        launched:"http://adlnet.gov/expapi/verbs/launched",
        initialized:"http://adlnet.gov/expapi/verbs/initialized",
        exited: "http://adlnet.gov/expapi/verbs/exited",
        abandoned: "http://adlnet.gov/expapi/verbs/abandoned",
        set: "https://example.com/relle/verbs/set",
        answered: "http://adlnet.gov/expapi/verbs/answered",
        completed: 'https://w3id.org/xapi/cmi5#completed',
        attempted: 'http://adlnet.gov/expapi/verbs/attempted'
    };
    
    return { "id":verbs[verb], "display":{"en-US":verb}};
}

function getObject(id, name, type){
    return { 
        "id":id, 
        "objectType":"Activity",
        "definition": {
            "name": {
                "en-US": name,
            },
            "type": type
        }
    };
}

function getContext(data){
    var context = {"extensions":{}};
    var loop = {};
    
        //for (var prop in data) {
        var prop = 'actuator';
            for (var prop2 in data[prop]) {
                for (var prop3 in data[prop][prop2]) {
                    var sum = parseInt(prop3)+1;
                    loop["https://example.com/relle/dcElectricPanel/"+prop+"/"+prop2+"/"+sum] = data[prop][prop2][prop3];
                }
            }
        //}
        context["extensions"]["https://example.com/relle/lab-information"] = loop;
        //console.log(context);
        return context;
}

function getActorUser(){
    return {
        "name": $('#user-data').data('name'),
        "mbox":$('#user-data').data('email')
    };
}

function getResult(data){
    var result = {"extensions":{}};
    var loop = {};
    
        //for (var prop in data) {
        var prop = 'sensor';
            for (var prop2 in data[prop]) {
                for (var prop3 in data[prop][prop2]) {
                    var sum = parseInt(prop3)+1;
                    loop["https://example.com/relle/dcElectricPanel/"+prop+"/"+prop2+"/"+sum] = data[prop][prop2][prop3];
                }
            }
        //}
        result.extensions["https://example.com/relle/lab-information"] = loop;
        //console.log(context);
        return result;
}


//end xAPI

    
 

$('head').append('<link rel="stylesheet" href="http://relle.ufsc.br/css/shepherd-theme-arrows.css" type="text/css"/>');

$.getScript('http://relle.ufsc.br/exp_data/1/1/welcome.js', function () {
    var shepherd = setupShepherd();
    addShowmeButton('<button id="btnIntro" class="btn btn-sm btn-default"> <span class="long">' + lang.showme + '</span><span class="short">' + lang.showmeshort + '</span> <span class="how-icon fui-question-circle"></span></button>')
    $('#btnIntro').on('click', function (event) {
        event.preventDefault();
        shepherd.start();
        //xapi send tutorial statement
        sendStatement({actor:getActorUser(), verb:'launched', obj:{id:window.location.href, name:'Tutorial', type:'tutorial'}});
    });
});

//var rpi_server = "http://paineldc1.relle.ufsc.br";
var instance = $('#selected').data('instance');
var rpi_server = 'http://' + $('#instance-'+instance).data('address');
console.log('URL: '+rpi_server);
var cam_snapshot = rpi_server + "/snapshot.jpg";
var results;
var socket = '';
var switches = 0;
var UIimg_interval = null;
var circuit_images = [10, 12, 13, 14, 17, 18, 20, 21, 24, 25, 28, 29, 30, 32, 34, 36, 37, 38, 4, 40, 41, 42, 44, 45, 48, 49, 5, 53, 54, 57, 58, 60, 61, 62, 64, 65, 66, 67, 68, 69, 7, 70, 72, 73, 74, 8, 80, 81, 9, 96, 97];
var image = '';
var labData = '';
var labName = "DC Electric Panel";

$(function () {

    $('#cam').attr('src', rpi_server + ':8080');


    //Setting xAPI LRS info
    labData = {
        actuator: {
            "switch": [0, 0, 0, 0, 0, 0]
        },
        sensor: {
            "amperemeter": [0, 0, 0, 0, 0, 0, 0],
            "voltmeter": [0, 0]
        }
    };
    ////function sendStatement(verb, objname, objtype, context)

    //xapi send launch statement
    sendStatement({actor:getActorUser(), verb:'initialized', obj:{id: window.location.href, name:labName, type:'laboratory'}, context: labData});
    
    //xapi send exit statement
    $('#btnLeaveExp').click(function(){
        sendStatement({actor:getActorUser(), verb:'exited', obj:{id:window.location.href, name:labName, type:'laboratory'}, context: labData});
    });

    $('.switch').bootstrapToggle({
        onstyle: "success",
        offstyle: "danger",
        size: "small"
    });    

    function reset() {
        var message = {};
        message.sw = {};
        message.pass = $('meta[name=csrf-token]').attr('content');

        for (var i = 0; i < 7; i++) {
            message.sw[i] = 0;
        }

        
        if (message && socket) {
            message.pass = $('meta[name=csrf-token]').attr('content');
            socket.emit('new message', message); 
        }
    }

    function sendMessage() {
        clearTimeout(UIimg_interval);
        switches = 0;
        var message = {};
        message.sw = {};
        message.pass = $('meta[name=csrf-token]').attr('content');
        for (var i = 0; i < 7; i++) {
            if ($("input[id='sw" + i + "']:checked").length) {
                message.sw[i] = 1;
                switches = switches | 1 << i;
                console.log('switches: ' + switches);
            } else {
                message.sw[i] = 0;
            }
        }
        if (message && socket) {
            message.pass = $('meta[name=csrf-token]').attr('content');
            socket.emit('new message', message);
        }
        UIimg_interval = setTimeout(function () {
            if (circuit_images.indexOf(switches) >= 0) {
                console.log('loading /exp_data/1/2/resultante/' + switches + '.png');
                $('.equivalent').attr('src', '/exp_data/1/2/equivalent/' + switches + '.png');
                $('.default_circuit').hide().addClass('hiddencircuit');
                $('.equivalent').show().removeClass('hiddencircuit');
            } else {
                $('.equivalent').hide().addClass('hiddencircuit');
                $('.default_circuit').show().removeClass('hiddencircuit');
                console.log('resultante ' + switches + ' nao criada ainda');
            }
        }, 3000);
    }

    $('.switch').change(function () {
        sendMessage();
        console.log(+($(this).prop('checked')));
        var swid = parseInt($(this).prop('id').slice(-1));
        labData.actuator.switch[""+swid] = (+($(this).prop('checked')));
        
        setTimeout(function () {
            sendStatement({actor: getActorUser(), verb: 'set', obj: {id: window.location.href, name: "Switch " + swid, type: 'sensor'}, context: labData, result: labData});
        }, 2000);
        
        //xapi Update data object
        //labData.actuators.switches = message.sw;
    });


    $.getScript(rpi_server + '/socket.io/socket.io.js', function () {
        // Initialize varibles
        // Prompt for setting a username
        socket = io.connect(rpi_server);
        
        socket.emit('new connection', {pass: $('meta[name=csrf-token]').attr('content')});
        
        socket.on('reconnect', function () {
            socket.emit('new connection', {pass: $('meta[name=csrf-token]').attr('content')} );
        });

        socket.on('reconnecting', function () {
            console.log('reconnecting');
        });
        
        $(".controllers").show();
        $(".loading").hide();

        socket.on('new message', function (data) {
            console.log(data);
        });


        // Whenever the server emits 'user joined', log it in the chat body
        socket.on('data received', function (data) {
            //printLog(data);
            results = $.parseJSON(data);
            
            switches = data;
            
            console.log("I'm receiving " + data);

            for (var i = 0; i < 7; i++) {
                if (!isNaN(results.amperemeter[i])) {
                    var val = parseFloat(results.amperemeter[i] / 1000).toFixed(1) + " mA";
                    $("#a" + i).html(val);
                    labData.sensor.amperemeter[i] = val;
                }
            }
            for (var i = 0; i < 2; i++) {
                if (!isNaN(results.voltmeter[i])) {
                    var val = parseFloat(results.voltmeter[i] / 1000).toFixed(2) + " V"
                    $("#v" + i).html(val);
                    labData.sensor.voltmeter[i] = val;
                }
            }
            
        });


    });
});

function LabLeaveSessionHandler() {
    if ($('.equivalent.hiddencircuit').length > 0) {
        image = $('img.default_circuit').attr('src');
    } else {
        image = $('.equivalent').attr('src');
    }
}

function report(id) {
    var array = results;
    array.equivalent = image;
    array.cam_url = cam_snapshot;
    console.log(array);
    //$.parseJSON(results);   //JSON formatado como variável results no topo
    //$.redirect('http://relle.ufsc.br/labs/'+ id + '/report', array);
    $.ajax({
        type: "POST",
        url: location.pathname + "/report/",
        data: array, // results, //{a2: 'i'}, // results,
        success: function (pdf) {
            var win = window.open(location.pathname + "/report", '_blank');
            //   win.focus();
            console.log("Report created.");
        }
    });
}

