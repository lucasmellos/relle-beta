<?php
/*
 * Social Login
 */
Route::get('auth/{provider}', 'Auth\AuthController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\AuthController@handleProviderCallback');

/*
 * Docs
 */
Route::group(array('prefix' => 'docs'), function() {
    Route::get('/create', ['middleware' => 'admin', 'uses' => 'DocsController@create']);
    Route::post('/create', ['middleware' => 'admin', 'uses' => 'DocsController@store']);
    Route::get('/{id}/edit', ['middleware' => 'admin', 'uses' => 'DocsController@edit']);
    Route::post('/{id}/edit', ['middleware' => 'admin', 'uses' => 'DocsController@doEdit']);
    Route::get('/{id}/delete', ['middleware' => 'admin', 'uses' => 'DocsController@delete']);
    Route::post('/{id}/delete', ['middleware' => 'admin', 'uses' => 'DocsController@doDelete']);
    Route::get('/all', 'DocsController@show');
});

//Search Bar  
Route::post('/search', 'HomeController@search');

Route::get('/new', 'HomeController@index');
/*
 * Booking
 */
Route::group(array('prefix' => 'booking'), function() {
    Route::get('/create', ['middleware' => 'teacher', 'uses' => 'BookingController@create']);
    Route::post('/create', ['middleware' => 'teacher', 'uses' => 'BookingController@doCreate']);
    Route::get('/all', ['middleware' => 'teacher', 'uses' => 'BookingController@show']);
    Route::get('/{booking}/delete', ['middleware' => 'auth', 'uses' => 'BookingController@delete']);
    Route::post('/{booking}/delete', ['middleware' => 'auth', 'uses' => 'BookingController@doDelete']);
    Route::get('/{booking}/edit', ['middleware' => 'auth', 'uses' => 'BookingController@edit']);
    Route::post('/{booking}/edit', ['middleware' => 'auth', 'uses' => 'BookingController@doEdit']); 
});

Route::get('/repositorio', 'RepositorioController@funcao');
Route::post('/searchpraticas', 'RepositorioController@searchpraticas');
Route::get('/create_practice', 'RepositorioController@criarpratica');
Route::post('/createpratica', 'RepositorioController@createpratica');


Route::get('/en', function(){
    session(['locale' => 'en']);
    return redirect()->back();
});
Route::get('/pt', function(){
    session(['locale' => 'pt']);
    return redirect()->back();
});
Route::get('/es', function(){
    session(['locale' => 'es']);
    return redirect()->back();
});

Route::get('/', 'HomeController@index');

Route::get('/labs', 'LabsController@labs_page');

Route::get('/login', 'PagesController@login');
Route::post('/login', 'Auth\AuthController@login');
Route::get('/logout', 'Auth\AuthController@logout');
Route::get('/login/forgot', 'Auth\AuthController@forgot');
Route::post('/login/forgot', 'Auth\AuthController@sendPass');
Route::get('/login/guest', 'Auth\AuthController@guest');

Route::get('/password/reset/{token}', 'Auth\AuthController@reset');

Route::get('cam', function() {
    //return view('welcome');
    return Request::server('HTTP_ACCEPT_LANGUAGE')[0] . Request::server('HTTP_ACCEPT_LANGUAGE')[1];
});

/*
 * Pages
 */
Route::get('/sobre', 'PagesController@about');
Route::get('/contato', 'PagesController@contact');

/*
 * Dashboard
 */
Route::get('/controle', ['middleware' => 'auth', 'uses' => 'PagesController@dashboard']);

/*
 * Pages
 */
Route::get('/about', 'PagesController@about');
Route::get('/contact', 'PagesController@contact');
Route::post('/contact', 'ContactController@contactForm');
/*
 * Dashboard
 */
Route::get('/dashboard', ['middleware' => 'auth', 'uses' => 'PagesController@dashboard']);

/*
 * Labs
 */
Route::group(array('prefix' => 'labs'), function() {
    Route::get('/', 'LabsController@labs_page');
//Instances
//Create Instance
    Route::get('/create/instance', ['middleware' => 'admin', 'uses' => 'LabsController@createInstance']);
//Store Instance
    Route::post('/create/instance', 'LabsController@storeInstance');
//Instance
    Route::get('/{id}/instance', ['middleware' => 'admin', 'uses' => 'LabsController@instance']);
    Route::get('{lab_id}/instance/{id}/delete', ['middleware' => 'admin', 'uses' => 'LabsController@delete']);

//Create Lab
    Route::get('/create', ['middleware' => 'admin', 'uses' => 'LabsController@create']);
    Route::get('/create/beta', ['middleware' => 'admin', 'uses' => 'LabsController@create2']);
//Store Lab
    Route::post('/create', 'LabsController@store');
// All Labs
    Route::get('/all', ['middleware' => 'admin', 'uses' => 'LabsController@all']);
//Edit
    Route::get('/{id}/edit', ['middleware' => 'admin', 'uses' => 'LabsController@edit']);
    Route::post('/{id}/edit', 'LabsController@doEdit');
//Delete
    Route::post('/dodelete', ['middleware' => 'admin', 'uses' => 'LabsController@doDelete']);
    Route::get('/{id}/delete', ['middleware' => 'admin', 'uses' => 'LabsController@delete']);

    //Moodle
    Route::get('/{id}/moodle', 'LabsController@moodle');
    //LabsLand
    Route::get('/{id}/labsland', 'LabsController@labsland');
    
    //Lab page
    Route::get('/{id}', ['middleware' => 'cors', 'uses' => 'LabsController@lab']);
    
     
    Route::post('/export', 'LabsController@export_csv');
    Route::get('/export', 'LabsController@export_csv');
    
    
    //Experiments
    Route::get('/{id}/exp/{expid}/edit', ['middleware' => 'admin', 'uses' => 'ExperimentsController@edit']);
    Route::get('/{id}/exp/{expid}', 'ExperimentsController@one');
     
});

Route::group(array('prefix' => 'exp'), function() {
    Route::get('/all', 'ExperimentsController@all');
    Route::get('/create', ['middleware' => 'admin', 'uses' => 'ExperimentsController@create']);
    Route::post('/create', ['middleware' => 'admin', 'uses' => 'ExperimentsController@save']);
    Route::post('/verify', 'ExperimentsController@verify');
});

Route::get('/users', 'UsersController@all');

Route::group(array('prefix' => 'users'), function() {
    Route::get('/create', ['middleware' => 'admin', 'uses' => 'UsersController@create']);
    Route::get('/edit', ['middleware' => 'auth', 'uses' => 'UsersController@edit']);
    Route::get('/delete', ['middleware' => 'auth', 'uses' => 'UsersController@delete']);
    Route::get('/{id}/delete', ['middleware' => 'admin', 'uses' => 'UsersController@deleteOther']);
    Route::post('/create', ['middleware' => 'admin', 'uses' => 'UsersController@store']);
    Route::post('/edit', ['middleware' => 'auth', 'uses' => 'UsersController@doEdit']);
    Route::post('/dodelete', ['middleware' => 'admin', 'uses' => 'UsersController@doDelete']);
    Route::get('/signup', 'UsersController@signUp');
    Route::post('/signup', 'UsersController@doSignUp');
    Route::get('/{id}/edit', ['middleware' => 'admin', 'uses' => 'UsersController@editOther']);
    Route::get('/import', 'UsersImportController@show');
    Route::post('/import', ['middleware' => 'admin', 'uses' => 'UsersImportController@import']);
    Route::get('/export', 'UsersImportController@export');
    Route::get('/bulk', ['middleware' => 'admin', 'uses' => 'UsersController@bulk']);
    Route::post('/admin', 'UsersController@admin');
    Route::post('/teacher', 'UsersController@teacher');
    Route::post('/delete/bulk', 'UsersController@delete_bulk');
    Route::post('/export/bulk', 'UsersImportController@export_bulk');
    
/*
 * xAPI User Info
 */
Route::post('/info', "UsersController@getUserInfo");

    
});

//Reports
Route::get('/labs/{id}/report', 'ReportsController@reports');
Route::post('/labs/{id}/report', 'ReportsController@reports_post');

Route::group(array('prefix' => 'log'), function() {
    Route::get('all', 'LogController@all');
});


Route::get('status', function() {
    $query = 'SELECT lab_id FROM instances WHERE maintenance = "1"';
    $status = DB::select($query);

    return $status;
});

Route::post('contact', 'ContactController@send');

Route::get('doc/{name}', function() {
    return "<script>"
            . "window.open(" . asset('doc/' . Route::getCurrentRoute()->parameters()[0]) . ",'_blank');"
            . "</script>";
});

Route::post('loggedin', function() {
    if (Auth::user()) {
        $info = [
                'avatar' => asset(Auth::user()->avatar),
                'name' => Auth::user()->firstname
            ];
        echo json_encode($info);
    } else {
        echo json_encode(false);
    }
});

Route::get('labx', function(){
    return redirect('/labs/5');
});

Route::group(array('prefix' => 'arduino'), function() {
    Route::post('upload', 'ArduinoController@upload');
    Route::post('download', 'ArduinoController@download');
    Route::post('get', 'ArduinoController@get');
});
