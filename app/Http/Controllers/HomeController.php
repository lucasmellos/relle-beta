<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Labs;
use App\Instances;
use App\Log;
use App\Subjects;
use App\SubjectsLabs;
use Input;
use DB;
use App;
use Auth;

class HomeController extends Controller {

    public function index() {
        
        $query = Instances::where('maintenance',0)->get();
        $labs = Labs::all();
        $disp = [];

        foreach($labs as $lab){
            foreach($query as $one){
                if($lab->id == $one->lab_id){
                    array_push($disp, $lab);
                }
            }
        }

        return view('home', compact('disp','labs'));
    }

    public function search() {
        
        $input = Input::all();

        $result = null;
        $first = true;
        $generalLab = '';
        $generalDoc = '';
        if (!empty($input['terms'])) {
            $string = $input['terms'];
            $terms = explode(' ', $string);
            $n = count($terms);
            $i = 1;
            foreach ($terms as $term) {
                $generalLab .= "name_pt like '%$term%' or "
                        . "name_en like '%$term%' or "
                        . "description_pt like '%$term%' or "
                        . "description_en like '%$term%' or "
                        . "tags like '%$term%'";
                if ($i < $n) {
                    $generalLab.=' or ';
                    $i++;
                }
            }
            foreach ($terms as $term) {
                $generalDoc .= "title like '%$term%' or "
                . "tags like '%$term%'";
                if ($i < $n) {
                    $generalDoc.=' or ';
                    $i++;
                }
            }
            $first = false;
        
            $queryLab = "select * from labs where " . $generalLab;
            $lab = DB::select($queryLab);
            $queryDoc = "select * from docs where " . $generalDoc;
            $doc= DB::select($queryDoc);
                
            $result['labs'] = $lab;
            $result['docs'] = $doc;
        }

        return view('search_results', compact('result'));
    }

}
