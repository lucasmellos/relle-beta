@extends ('layout.dashboard')

@section('inside-head')
<link rel="stylesheet" type="text/css" href="{{asset('css/jquery.steps.css')}}">
<style>
    .entry:not(:first-of-type){
        margin-top: 10px;
    }
    .input-group-btn{
        margin-top:-15px;
    }
    .gap{
        background-color: gold;
    }
    .error-opt{
        color:red;
    }
</style>
@stop

@section('page')
{{trans('interface.name', ['page'=>trans('experiments.create')])}}
@stop

@section ('inside')
<h4>{{trans('experiments.create')}}</h4>

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <form id="wizard" action="{{url('exp/create')}}" method="post">
            <!--TODO: TRADUZIR-->
            <h3>Descrição</h3>
            <section>
                <div class="row">
                    <div class="col-lg-6">
                        <label for='lab'>Laboratório</label><br>
                        <select class="form-control select select-default" name="lab-id" id="lab-id">
                            @foreach($labs as $one)
                            <option value="{{$one->id}}">{{$one->name_pt}}</option>
                            @endforeach 
                        </select>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                        <label for="exampleInputFile">Selecionar imagem</label>
                        <input type="file" id="img" name="img">
                        <p class="help-block">Insira uma thumbnail para o experimento</p>
                      </div>
                    </div>
                </div>
                

                <br>
                <label for='name_pt'>Nome</label>
                <input name='name_pt' id='name_pt' class='form-control' type='text'/>
                <label for='descricao_pt'>Descrição</label>
                <input name='descricao_pt' id='descricao_pt' class='form-control' type='text'/>
                <label for='tags'>Tags</label>
                <input name='tags' class='form-control' id="tags" type='text'/>
            </section>
            <h3>Questões</h3>
            <section>               
                <a href="#" class="btn btn-primary btn-lg" id="add" type="button">
                        +</a>
                <div class="modal fade" id="modal-add" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Criar Questão</h4>
                            </div>
                            <div class="modal-body">
                                <select class="form-control select select-default" name="type" id="type">
                                    <option value="multiple">Múltipla Escolha</option>
                                    <option value="gap">Preencha as Lacunas</option>
                                    <option value="number">Número</option>
                                </select>
                                <a href="#" class="btn btn-primary btn-lg" id="type-selected" type="button">
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                </a>
                                <br><br>
                                <div id="options-container">
                                    
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" id="close-modal-add" data-dismiss="modal">Cancelar</button>
                                <button type="button" class="btn btn-default" onclick="saveQuestion()">Salvar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </form>


    </div>
</div>
@stop

@section('script_dash')
<script src="{{asset('js/jquery.steps.js')}}"></script>
<script src="{{asset('js/jquery.lettering.js')}}"></script>

<script>
questions = {};
types = {
    'multiple': 'Múltipla Escolha',
    'gap': 'Preencha as Lacunas',
    'number': 'Número'
};

    $(function () {
        var form = $("#wizard");
        form.steps({
            headerTag: "h3",
            bodyTag: "section",
            transitionEffect: "slideLeft",
            autofocus: true,
            labels: {
                cancel: "Cancelar",
                finish: "Salvar",
                next: "Próximo",
                previous: "Voltar",
                loading: "Carregando ..."
            },
            onInit: function () {
                $("select").select2();
                $('#s2id_lab-id').remove();
                $('#s2id_type').remove();
                $(".dropdown-toggle").dropdown();
                
                $('#add').click(function(e) {
                    e.preventDefault()
                    $('#modal-add').modal({
                        show: true, 
                        backdrop: 'static',
                        keyboard: true
                     });
                });
               
                $('#type-selected').click(function(){
                    switch($('#type').val()){
                        case 'multiple':
                            $('#options-container').html('');
                            optionsMultiple();
                            $('input[type=checkbox]').on('change', function() {
                                $('input[type=checkbox]').not(this).prop('checked', false);  
                            });
                            break;
                        case 'gap':
                            $('#options-container').html('');
                            optionsGap();
                            break;
                        case 'number':
                            $('#options-container').html('');
                            optionsNumber();
                            break;
                    }
                });
                
                
                $('#save-question').click(saveQuestion());
            },
            onFinishing: function (event, currentIndex) { 
                if(Object.keys(questions).length){
                    return true;
                }else{
                    return false;
                }
            },
            onFinished: function (event, currentIndex) {
                var data = new FormData();
                
                data.append('_token',$('meta[name="csrf-token"]').attr('content'));
                
                data.append('labid',$('#lab-id').val());
                data.append('name_pt',$('#name_pt').val());
                data.append('description_pt',$('#descricao_pt').val());
                data.append('tags', $('#tags').val());
                data.append('img', $('#img')[0].files[0]);
                
                data.append('questions', JSON.stringify(questions));
                
                $.ajax({
                    url:"{{url('exp/create')}}", 
                    type:'post',
                    data:data,
                    mimeType: "multipart/form-data",
                    processData: false,      
                    contentType: false
                }).done(function( msg ) {
                    
                });
            }
        });
    });



    $('.btnNext').click(function () {
        $('.nav-tabs > .active').next('li').find('a').trigger('click');
    });

    $('.btnPrevious').click(function () {
        $('.nav-tabs > .active').prev('li').find('a').trigger('click');
    });
    $(function ()
    {
        $(document).on('click', '.btn-add', function (e)
        {
            e.preventDefault();

            var controlForm = $('.controls'),
                    currentEntry = $(this).parents('.entry:first'),
                    newEntry = $(currentEntry.clone()).appendTo(controlForm);

            newEntry.find('input').val('');
            controlForm.find('.entry:not(:last) .btn-add')
                    .removeClass('btn-add').addClass('btn-remove')
                    .removeClass('btn-success').addClass('btn-danger')
                    .html('<span class="glyphicon glyphicon-minus"></span>');
        }).on('click', '.btn-remove', function (e)
        {
            $(this).parents('.entry:first').remove();

            e.preventDefault();
            return false;
        });
    });  
    
    function optionsNumber(){
        var options = " \
            <div class='row'> \
                <div class='col-lg-12'> \
                    Questão <textarea class='form-control' id='question' name='question'> </textarea> \
                </div> \
                <div class='col-lg-12'> \
                    Resposta <input type='number' class='form-control' name='answer' id='answer'> \
                </div> \
            </div>";
        $('#options-container').append(options);
    }
    
    function optionsGap(){
        var options = " \
                <div class='row'> \
                    <div class='col-lg-12'>  \
                        Questão <textarea class='form-control' name='question' id='question'> </textarea> \
                    </div>  \
                    <div class='col-lg-12'>  \
                        Texto <textarea class='form-control' name='text' id='text'> </textarea> \
                    </div>  \
                    <div class='col-lg-12'>  \
                        <button type='button' class='btn btn-xs pull-right' onclick='selectGaps()'>Selecionar Lacunas</button> \
                    </div>  \
                    <div class='col-lg-12'> \
                        <p id='text-option'></p> \
                    </div> \
                </div>";
        $('#options-container').append(options);
    }
    
    function selectGaps(){
        
        $('#text-option').html('');
        $('#text-option').before('<p style="font-size:10pt; font-weight:bold">Clique nas palavras que serão usadas como lacunas</p>');
        $('#text-option').append($('#text').val());
        $('#text-option').lettering('words');
        $('#text-option>span').click(function(){
            $(this).addClass('gap');
        });
        
    }
    
    
    function optionsMultiple(){
        var options = " \
                <div class='row'> \
            <div class='col-lg-12'> \
                Questão <textarea class='form-control' id='question' name='question'> </textarea> \
            </div> \
        </div> \
        <div class='row'> \
            <div class='col-lg-12'> \
                <br> \
                <strong>Opções</strong> \
                <br> \
            </div> \
            <div class='col-lg-5'id='text-input'> \
                Texto <input type='text' class='form-control' id='text-option'> \
            </div> \
            <div class='col-lg-4' id='value-input'> \
                Valor <input type='text' class='form-control' id='value-option'> \
            </div> \
            <div class='col-lg-2'> \
                <br> \
                <a href='#' class='btn btn-primary btn-sm' onclick='appendOption()' type='button'>+</a> \
            </div> \
        </div> \
        <div class='row'> \
            <div class='col-lg-12'> \
                <br><br> \
                <table class='table table-condensed table-responsive' id='multiple-options'> \
                    <colgroup> \
                        <col width='20%' style='padding-left:10px'>\
                    </colgroup>\
                    <tr>\
                        <th>Correta</th>\
                        <th>Texto</th>\
                        <th>Valor</th>\
                    </tr>\
                </table>\
            </div>\
        </div>";
        $('#options-container').append(options);
        
        
    }
    
    function appendOption(){
        $('.error-opt').remove();
        var text = $('#text-option').val();
        var val = $('#value-option').val();
        var flag = 0;
        
        $('.value-col').each(function(){
            if($(this).html()==val){
                $('#value-input').append("<span class='error-opt'>Não são permitidas opções com o mesmo valor.</span>");
                $('#text-option').val('');
                $('#value-option').val('');
                flag=1;
            }
        });
        $('.text-col').each(function(){
            if($(this).html()==text){
                $('#text-input').append("<span class='error-opt'>Não são permitidas opções com o mesmo texto.</span>");
                $('#text-option').val('');
                $('#value-option').val('');
                flag=1;
            }
        });
        if(flag) return;
        
        var row = " \
        <tr class='option-row'> \
            <td><input type='checkbox'></td> \
            <td class='text-col'>"+text+"</td> \
            <td class='value-col'>"+val+"</td> \
        </tr>";
        
        $('#multiple-options').append(row);
        $('#text-option').val('');
        $('#value-option').val('');
        
    }
    
    function saveQuestion(){

        switch($('#type').val()){
            case 'multiple':
                if($('input[type=checkbox]:checked').length){
                    $('.error-opt').remove();
                    getInfoMultiple();
                }else if($('input[type=checkbox]:checked').length > 1){
                    $('#multiple-options').before("<span class='error-opt'>Escolha somente uma resposta correta</span>");
                }else{
                   $('#multiple-options').before("<span class='error-opt'>Escolha a resposta correta</span>");
                }
                break;
            case 'gap':
                getInfoGap();
                break;
            case 'number':
                var qst = {
                    type: $('#type').val()
                };
                qst['question'] = $('#question').val();
                qst['answer'] = $('#answer').val();
                var qtd = Object.keys(questions).length+1;
                var key = 'num'+qtd;
                questions[key] = qst;    
                appendToPage(key);
                console.log(questions);
                break;
        }
              
        
        $('#close-modal-add').click();
        $('#options-container').html('');
        
    }
    
    function getInfoGap(){
        
        var text='';
        var gaps = [];
        
        $('[class^="word"]').each(function(){
            if($(this).hasClass('gap')){
                text +=' '+'<span class="tag"></span>';
                gaps.push($(this).html());
            }else{
                text +=' '+$(this).html();
            }
        });
        
        var qst = {
            type: $('#type').val(),
            question: $('#question').val(),
            text: text,
            gaps: gaps
        };
        var qtd = Object.keys(questions).length+1;
        var key = 'gap'+qtd;
        questions[key] = qst;    
        appendToPage(key); 
    }
    
    function getInfoMultiple(){
        var qst = {
            type: $('#type').val(),
            question: $('#question').val()
        }
        var options = [];
        var count = 1;
        var answer;
        
        $('.option-row').each(function(){
            var opt = {};
            var el = $('<div></div>');
            el.html($(this));
            opt['text'] = $('.text-col', el).html();
            opt['value'] = $('.value-col', el).html();
            
            if($('input[type=checkbox]', el).prop('checked')){
                answer = $(this).val();
            }    
            options[count]=opt;
            count++;
        });

        qst ['answer'] = answer;
        qst ['options'] = options;
        
        //console.log(qst);
        
        var qtd = Object.keys(questions).length+1;
        var key = 'multiple'+qtd;
        questions[key] = qst;    
        appendToPage(key);
        
        console.log(questions);
    }
    
    function appendToPage(id){
        console.log(id);
        console.log(questions);
        if(Object.keys(questions).length==1){
            var txt = "<div class='row' id='header-table'> \
                <div class='col-lg-12'> \
                    <br><br> \
                    <table class='table table-condensed table-responsive' id='list-questions'> \
                        <colgroup> \
                            <col width='20%' style='padding-left:10px'>\
                        </colgroup>\
                        <tr id='qst-table-header'>\
                            <th>Tipo</th>\
                            <th>Questão</th>\
                            <th>Ações</th>\
                        </tr>\
                    </table>\
                </div>\
            </div>";
            $('#add').after(txt);
        }
        var row = "<tr id='"+id+"'> \
                    <td>"+types[questions[id].type]+"</td> \
                    <td>"+questions[id].question+"</td> \
                    <td><a href='#' onclick="+'"'+
                    "deleteQuestion('"+id+"')"+'"'+"><i class='fa fa-trash-o' aria-hidden='true'></i></a></td> \
                </tr>";
        $('#qst-table-header').after(row);
    }
    
    function deleteQuestion(id){
        console.log("delete questions."+id);
        
        var tempQst = {};
        Object.keys(questions).forEach(function(key){
            if(key!==id){ 
                tempQst[key] = questions[key];
            }
        });
        console.log(tempQst);
        questions = tempQst;

        //delete(questions.id);
        $('#'+id).remove();
        if(Object.keys(questions).length<1) $('#header-table').remove();
        
    }
</script>
@stop

