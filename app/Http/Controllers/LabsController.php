<?php

namespace App\Http\Controllers;

use App\Labs;
use App\Docs;
use App\DocsLabs;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Request;
use Image;
use Imagine\Image\Box;
use File;
use DB;
use App;
use Route;
use Input;
use Validator;
use App\Log;
use Auth;
use Excel;
use ExcelFile;
use App\Instances;
use ZipArchive;

//use App\Http\Requests\CreateLabFormRequest;

/**
 * Labs Controller
 *
 * The Labs Controller class is responsible for handling requests to the Labs database, in addition to managing the Labs views.
 * 
 * @category Controllers
 * @package App\Http\Controllers
 * @license http://opensource.org/licenses/MIT MIT License
 *
 * @version 1.0
 * @author José Simão <josepedrosimao@gmail.com>
 */
class LabsController extends Controller {

    /**
     * @return \Illuminate\View\View
     */
    public function show() {

        $labs = Labs::all();
        return view('labs.all', compact('labs'));
    }

    public function labs_page() {

        $query = Instances::where('maintenance',0)->get();
        $labs = Labs::all();
        $disp = [];

        foreach($labs as $lab){
            foreach($query as $one){
                if($lab->id == $one->lab_id){
                    array_push($disp, $lab);
                    $disp=array_unique($disp);
                }
            }
        }
        
        $user = Auth::user();
        //dd($disp);
        return view('all_labs', compact('disp', 'user'));
    }

    /**
     * @return \Illuminate\View\View
     */
    public function create() {

        return view('labs.create');
    }
    
    
     public function create2() {

        return view('labs.create_new');
    }
    /**
     * Stores Labs data
     *
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store() {

        $input = Input::all();

        $rules = [
            'name_pt' => 'required',
            'name_en' => 'required',
            'name_es' => 'required',
            'description_pt' => 'required',
            'description_en' => 'required',
            'description_es' => 'required',
            'tags' => 'required',
            'duration' => 'required',
            'target' => 'required',
            'subject' => 'required',
            'difficulty' => 'required',
            'interaction' => 'required',
            'thumbnail' => 'required',
            //'package' => 'required',
        ];
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {

// get the error messages from the validator
            $messages = $validator->messages();

// redirect our user back to the form with the errors from the validator
            return redirect('labs/create')
                            ->withInput()
                            ->withErrors($validator);
        } else {

//return var_dump($input);
//Image handling
            $tmp_path = $input['thumbnail']->getRealPath();
            
            $extension = $input['thumbnail']->getClientOriginalExtension();

            $path = '/img/exp/' . uniqid() . '.' . $extension;
            Image::make($tmp_path)->save(base_path() . '/public' . $path);
//Image::make($tmp_path)->resize(240, 180)->save(base_path().'/public'.$path);
            $input['thumbnail'] = $path;


//Array
            $input['target'] = implode(',', $input['target']);
            $input['subject'] = implode(',', $input['subject']);
            if(sizeof(Labs::all())>0){
                $last = Labs::orderBy('id', 'desc')->first()->id;
                $input['id'] = $last+1;    
            }else{
                $input['id']=0;
            }
            //dd($input);
//Package

       Labs::create($input);
       return redirect('labs/create/instance');
   }

    }
    /**
     * @return \Illuminate\View\View
     */
    public function search() {

        $input = Input::all();

        if (
                empty($input['terms']) &&
                !array_key_exists('target', $input) &&
                !array_key_exists('subject', $input) &&
                !array_key_exists('difficulty', $input) &&
                !array_key_exists('interaction', $input)
        ) {

            return $this->show();
        } else {
            $labs = searchLabs($input);

            return view('all_labs', compact('labs'));
        }
    }

    /**
     * @return \Illuminate\View\View
     */
    public function lab() {
        $id = Route::getCurrentRoute()->parameters();
        $exp = Labs::find($id)[0];
        $exp['lang'] = App::getLocale();


        $array = DocsLabs::where('lab_id', $id)->get();
        $docs = ['tec' => [], 'did' => []];

        foreach ($array as $one) {
            $doc = Docs::find($one['doc_id'])->toArray();
            if ($doc['type'] == 'manual') {
                array_push($docs['tec'], $doc);
            } else {
                array_push($docs['did'], $doc);
            }
        }
        $suggestions = Labs::where('subject', $exp->subject)
                            ->where('id','!=', $exp->id)
                            ->inRandomOrder()->limit(4)->get();

        if ($exp['maintenance'] == 1) {
            if (Auth::check()) {
                if (!admin()) {
                    return redirect('/');
                } else {
                    return view('labs.one', ['exp' => $exp, 'docs' => $docs, 'suggestions' => $suggestions]);
                }
            } else {
                return redirect('/');
            }
        } else {

            return view('labs.one', ['exp' => $exp, 'docs' => $docs, 'suggestions' => $suggestions]);
        }
    }

    /**
     * @return \Illuminate\View\View
     */
    public function all() {
        $labs = Labs::all();
        $user = Auth::user()->type;
        return view('labs.all_dash', compact('labs', 'user'));
    }

    /**
     * @return \Illuminate\View\View
     */
    public function edit() {
        $id = Route::getCurrentRoute()->parameters()['id'];
        $data['lab'] = Labs::find($id);
        $data['docs'] = Docs::all();
        $data['instances'] = Instances::where('lab_id', $id)->get();
        
        //var_dump($data['instances']);

        $array = DocsLabs::where('lab_id', $id)->get();
        $docs = '';
        foreach ($array as $one) {
            $docs.=$one['doc_id'] . ', ';
        }
        
        $data['labs_docs'] = $docs;
        return view('labs.edit', compact('data'));
    }

    /**
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function doEdit() {
        $input = Input::all();
        $lab = Labs::where('id',$input['id'])->first();
        $instance = Instances::where('lab_id',$input['id'])->first();

        //Image handling
        if (empty($input['thumbnail'])) {
            $input['thumbnail'] = $lab->thumbnail;
        } else {
            $tmp_path = $input['thumbnail']->getRealPath();
            $extension = $input['thumbnail']->getClientOriginalExtension();

            $path = '/img/exp/' . uniqid() . '.' . $extension;
            //Image::make($tmp_path)->save(base_path() . '/public' . $path);
            Image::make($tmp_path)->resize(new Box(300, 300))->save(base_path() . '/public' . $path);
            $input['thumbnail'] = $path;
        }


        //Array
        $input['target'] = implode(',', $input['target']);
        $input['subject'] = implode(',', $input['subject']);

        //Package
        foreach($instance as $instances){
            if (!empty($input['package'.$instances])) {   
                $pathToZip=$input['package'.$instances]->getRealPath();
                $expId=$lab->id;
                $instanceId=$instances;
            
                $zip = new ZipArchive;
                $res = $zip->open($pathToZip);
                $path = "/public/exp_data/" . $expId ."/". $instanceId;
                $final_path = base_path() . $path;
                $reports_path = base_path() . "/resources/views/reports/" . $expId ."/". $instanceId;
            
                shell_exec("mkdir -p " . $final_path);
                shell_exec("mkdir -p " . $reports_path); //Reports
            
                if ($res === TRUE) {
                    $zip->extractTo($final_path);
                    shell_exec("mv " . base_path() . $path . "/report_en.blade.php " . $reports_path . "/report_en.blade.php"); //Reports
                    shell_exec("mv " . base_path() . $path . "/report_pt.blade.php " . $reports_path . "/report_pt.blade.php"); //Reports
                    $zip->close();
                } else {
                    $zip->close();
                    echo "Unable to unzip your zip archive.";
                }
                unset($input['package'.$instances]);
            }
        }
        unset($input['_token']);
        
        // Maintenance and queue switchies
        foreach($instance as $instances){
            if (array_key_exists("maintenance".$instances, $input))
                ($input['maintenance'.$instances] == 'on') ? $input['maintenance'.$instances] = '1' : $input['maintenance'.$instances] = '0';
            else
                $input['maintenance'.$instances] = '0';
            if (array_key_exists("queue".$instances, $input))
                ($input['queue'.$instances] == 'on') ? $input['queue'.$instances] = '1' : $input['queue'.$instances] = '0';
            else
                $input['queue'.$instances] = '1';
            }
            // Docs
            if (!empty($input['docs'])) {
                $old = DocsLabs::where('lab_id', $input['id'])->delete();
                $docs = explode(',', $input['docs']);
                if (!empty($docs)) {
                    foreach ($docs as $doc) {
                        $doclab = new DocsLabs();
                        $doclab->doc_id = $doc;
                        $doclab->lab_id = $input['id'];
                        $doclab->save();
                    }
                }
        }
        unset($input['docs']);
            
        //Instances table update
        
        foreach ($instance as $field){
            
            $instances= Instances::where(['lab_id'=> $lab, 'id' => $field])->get();
            foreach($instances as $instance){
                $instance->description= $input['description'.$field];
                $instance->address= $input['address'.$field];
                $instance->duration= $input['duration'];
                $instance->maintenance= $input['maintenance'.$field];
                $instance->save();
            }
            
            // Push instances elements on the array of input
            unset($input['description'.$field]);
            unset($input['address'.$field]);
            unset($input['queue'.$field]);
            unset($input['maintenance'.$field]);
        }
        
        //Labs table update
        Labs::where('id', '=', $input['id'])->update($input);
        return redirect('labs');
    }

    /**
     * @return \Illuminate\View\View
     */
    public function delete() {
        $id = Route::getCurrentRoute()->parameters()['id'];

        return view('labs.delete', compact('id'));
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function doDelete() {
        $req = Input::all();
        $lab = Labs::where('id',$req['id'])->first();

        /*      Exclusão dos arquivos no servidor       */
//        File::delete(public_path() . $lab->thumbnail);
//        File::deleteDirectory(public_path() . '/exp_data/' . $lab->id);
//        File::deleteDirectory(base_path() . '/resources/views/reports/' . $lab->id);

        $lab->delete();
        return redirect('labs/all');
    }

    /**
     * @return \Illuminate\View\View
     */
    function moodle() {
        $id = Route::getCurrentRoute()->parameters();
        $exp = Labs::find($id)[0];
        $exp['lang'] = App::getLocale();
        return view('labs.moodle', compact('exp'));
    }

    /**
     * @return \Illuminate\View\View
     */
    function labsland() {
        $id = Route::getCurrentRoute()->parameters();
        $exp = Labs::find($id)[0];
        $exp['lang'] = App::getLocale();
        return view('labs.labsland', compact('exp'));
    }
    /**
     * @param $id
     * @return bool
     */

    function status($id) {
        $query = 'SELECT id FROM labs WHERE maintenance = "1"';
        $out = DB::select($query);
        foreach ($out as $one) {
            if ($one->id == $id) {
                return false;
            }
        }
        return true;
    }
    function export_csv() {
        Excel::create('data', function($excel) {
            $excel->sheet('data sheet', function($sheet) {
                $sheet->fromArray(Input::all());
            });
        })->export('csv');
        header("Content-type: text/plain");
        header("Content-Disposition: attachment; filename=data.csv");
    }

    public function createInstance() {
        $nameLang = "name_".App::getLocale();
        $labs = Labs::pluck($nameLang, 'id');
        return view('labs.create_instance', compact('labs'));
    }
    
    /**
     * Stores Instance data
     *
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function storeInstance()
    {
        /*
       test instances after to do all
        */

        $input = Input::all();

        $rules = [
            'lab_id' => 'required',
            'description' => 'required',
            'address' => 'required',
            'duration' => 'required',
        ];

        $validator = Validator::make($input, $rules);
        if ($validator->fails() or empty($input['package'])) {
           // get the error messages from the validator
           $messages = $validator->messages();
           //redirect our user back to the form with the errors from the validator
           return redirect('labs/create')
                           ->withInput()
                           ->withErrors($validator);
        }else{
            //dd($input);
            // Unzip this instance
            $zip = new ZipArchive;
            $pathToZip=$input['package']->getRealPath();
            $res = $zip->open($pathToZip);
            $expId=$input['lab_id'];
            if(count(Instances::where('lab_id', $input['lab_id'])->orderBy('id', 'desc')->first())>0){
                $last=Instances::where('lab_id', $input['lab_id'])->orderBy('id', 'desc')->first();
                $instanceId= $last->id + 1;
            }else{
                $instanceId= 1;
            }
            
            $path = "/public/exp_data/" . $expId ."/". $instanceId;
            $final_path = base_path() . $path;
            $reports_path = base_path() . "/resources/views/reports/" . $expId ."/". $instanceId;
        
            shell_exec("mkdir -p " . $final_path);
            shell_exec("mkdir -p " . $reports_path); //Reports
            
            if ($res === TRUE) {
                $zip->extractTo($final_path);
                shell_exec("mv " . base_path() . $path . "/report_en.blade.php " . $reports_path . "/report_en.blade.php"); //Reports
                shell_exec("mv " . base_path() . $path . "/report_pt.blade.php " . $reports_path . "/report_pt.blade.php"); //Reports
                $zip->close();
            } else {
                $zip->close();
                echo "Unable to unzip your zip archive.";
            }
        }
           
        $input['id'] =  $instanceId;
        // Maintenance executation
        if (array_key_exists("maintenance", $input)) {
            ($input['maintenance'] == 'on') ? $input['maintenance'] = '1' : $input['maintenance'] = '0';
        } else {
            $input['maintenance'] = '0';
        }

        if (array_key_exists("queue", $input)) {
            ($input['queue'] == 'on') ? $input['queue'] = '1' : $input['queue'] = '0';
        } else {
            $input['queue'] = '0';
        }
        //dd($input);
        Instances::create($input);
        return redirect('labs');
    }
    


}
