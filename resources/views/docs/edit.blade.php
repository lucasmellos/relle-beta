@extends ('layout.dashboard')

@section('page')
{{trans('interface.name', ['page'=>trans('docs.edit')])}}
@stop

@section('title_inside')
{{trans('docs.edit')}}
@stop

@section ('inside')

<?php 
//Pegar atributos do objeto---------------------------------------------------//
$lang = $doc->{'lang'};
$type = $doc->{'type'};

//----------------------------------------------------------------------------//
?>

<style>
    .btn-file{
        border-radius: 5px;
        margin:3px;
    }
    .bootstrap-tagsinput{
        border: 2px solid #bdc3c7;
    }
    .bootstrap-tagsinput:focus{
        border: 2px solid #1abc9c;
    }
</style>

{!!
Form::open([
'files' => true,
'enctype'=> 'multipart/form-data',
'class' => 'dropzone'
])
!!}


<div class="row">
    <div class="form-group col-md-6 col-sm-12">
        <label>{{trans('docs.title')}}</label>
        <input type="text" name="title" class="form-control" required value="{{$doc->title}}"/>
    </div>

    <div class="form-group col-md-3 col-sm-12">
        <label>{{trans('docs.type')}}:</label>
        <select class="form-control select select-default" name="type" id="type_op">
            <optgroup label="{{trans('docs.technical')}}">
                <option value="manual">{{trans('docs.manual')}}</option>
            </optgroup>
            <optgroup label="{{trans('docs.didactic')}}">
                <option value="teaching" <?php if($doc->type == 'teaching'){echo 'selected';} ?>>{{trans('docs.teaching')}}</option>
                <option value="plan"     <?php if($doc->type == 'plan'){echo 'selected';} ?>>{{trans('docs.plan')}}</option>
                <option value="guide"    <?php if($doc->type == 'guide'){echo 'selected';} ?>>{{trans('docs.guide')}}</option>
            </optgroup>
        </select>
    </div>
</div>
<div class="row">
    <div class="form-group col-md-6 col-sm-12">
        <label>Tags</label>
        <input name="tags" class="tagsinput form-control" data-role="tagsinput" value="{{$doc->tags}}"/>
    </div>
    <div class="form-group col-md-3 col-sm-12">
        <label>{{trans('docs.lang')}}:</label>
        <select class="form-control select select-default " name='lang'>
            <option <?php if($doc->lang == 'en'){echo 'selected';} ?> value="en">{{trans("interface.en")}}</option>
            <option <?php if($doc->lang == 'pt'){echo 'selected';} ?>  value="pt">{{trans("interface.pt")}}</option>
        </select>        
    </div>
</div>
<div class="row">
    <div class="form-group col-md-12">
        <button class="btn btn-danger">{{trans("interface.save")}}</button>
        <button class="btn btn-success" type="button" onclick="window.history.back();">{{trans("interface.cancel")}}</button>
    </div>
</div>
{!!Form::close()!!}
@stop
