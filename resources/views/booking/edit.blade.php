@extends ('layout.dashboard')

@section('page')
{{trans('interface.name', ['page'=>trans('book.title_edit')])}}
{{$name='name_'.App::getLocale()}}
@stop

@section ('title_inside')
{{trans('book.title_edit')}}

<link href="{{asset('css/datepicker.css')}}" rel="stylesheet">
<link href="{{asset('css/bookingCreate.css')}}" rel="stylesheet">
<?php $horaServidor = date('H:i:s') ?>

@stop

@section ('inside')
<!------------------------------Mensagens de Erro---------------------------------->
<div id='error1' style='display:none' class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>{{ trans('book.error1') }}</strong>&nbsp{{ trans('book.durac2') }}
</div>
<div id='error2' style='display:none' class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>{{ trans('book.error2') }}&nbsp</strong> 
</div>
<div id='error3' style='display:none' class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>{{ trans('book.error3') }}</strong>
</div>
<div id='error4' style='display:none' class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>{{trans('book.sorry')}},</strong>&nbsp{{ trans('book.error4') }} 
</div>
<div id='error5' style='display:none' class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>{{trans('book.please')}},</strong>&nbsp{{ trans('book.error5') }}
</div>
<!-------------------------------------------------------------------------------->


<!-----------------------Inicio da Estrutura da página---------------------------->
<div class="row">
    <div class="form-group col-lg-4 col-md-6 col-sm-12" id="linha1">
        <label>{{trans('book.exp')}}</label>
        <select class="form-control select select-default" name="type" id="exp_id" onchange="mudarDescricao()">
            @foreach($labs as $lab)
                <option data-duration="{{$lab->duration}}" value="{{$lab->id}}">{{$lab->$name}}</option>
            @endforeach
        </select>
        <small>{{trans('book.small1')}}<span id="small1">8</span>{{trans('book.min')}}</small>
    </div>
    <div class="form-group col-lg-4 col-sm-12" id="linha1">
        <label>{{trans('book.duration')}}</label>
        <select class="form-control select select-default" id="duration2">
            <option  value="nse">{{trans("book.nse")}}</option>
            <option  value="nmin" selected="selected">{{trans("book.nmin")}}</option>
        </select>
    </div>
    <div class="form-group col-lg-2 col-sm-12" id="linha1">
        <input type=number min="1" max="120" class="form-control" id="durationBox" value="{{$booking->duration}}" style="margin:39px 0px; text-align: left">
    </div>
</div>

<div class="row" >
    <div class="form-group col-lg-4 col-sm-12">
        <label>{{trans('book.date')}}</label>
        <input type="text" value="{{$booking->date}}" id="datepickerID" class="form-control">
    </div>
    <div class="form-group col-lg-4 col-sm-12">
        <label>{{trans('book.time')}}</label>
        <input type="time" class="form-control" value="{{substr($booking->time,0,-3)}}" id="timepickerID">
        <small>{{trans('book.small2')}}</small>
    </div>
    <div class="form-group col-lg-2 col-sm-12">
        <label>{{trans('book.token')}}</label>
        <label class="form-control" style="text-align: center; font-weight: 900;background-color: #ccc; opacity: 0.6">{{$booking->token}}</label>
    </div>
</div>

<div class="row">
    <div class="form-group col-md-12">
        <button onclick="calcDuration();submitOutput()" class="btn btn-danger">{{trans("interface.save")}}</button>
        <button class="btn btn-success" type='button' onclick="window.history.back();">{{trans('interface.cancel')}}</button>
    </div>
</div>
@stop

@section ('script_dash')
<script src="{{asset('js/BookingCreate.js')}}" rel="text/javacript"></script>
<script src="{{asset('js/datepicker.js')}}"></script>
<script>
    var x = {{$booking->lab_id}};    
    var op = $("#exp_id option:eq("+(x-1)+")")[0];
    op.setAttribute('selected','selected');
</script>
@stop