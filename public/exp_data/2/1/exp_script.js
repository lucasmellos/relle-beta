/*
 * xAPI Compliant
 * Last edited: Simão -  17/11/2017
 */

$('head').append('<link rel="stylesheet" href="http://relle.ufsc.br/css/shepherd-theme-arrows.css" type="text/css"/>');

var rpi_server = "http://150.162.233.200";
var socket = '';
var circuit_images = [0, 7, 11, 13, 14, 15];
var UIimg_interval = null;

var labData;

$.getScript('http://relle.ufsc.br/exp_data/2/1/welcome.js', function () {
    var shepherd = setupShepherd();
    addShowmeButton('<button id="btnIntro" class="btn btn-sm btn-default"> <span class="long">' + lang.showme + '</span><span class="short">' + lang.showmeshort + '</span> <span class="how-icon fui-question-circle"></span></button>')
    $('#btnIntro').on('click', function (event) {
        event.preventDefault();
        shepherd.start();
        xapiCreateStatement({verb:'viewed', obj:{id:window.location.href, name:{'en-US':'Tutorial'}}, type:'tutorial'});
    });
});

$(function () {
    $('.switch').bootstrapToggle({
        onstyle: "success",
        offstyle: "danger",
        size: "small"
    });
    
    $("#btnLeaveExp").on("click", function() {
        xapiCreateStatement({verb:'exited', obj:{id:window.location.href, name:xapiObjName(), type:'laboratory'}});
    });
    
    labData = {
        "https://example.com/relle/acEletricPanel/actuator/switch": {
                1:0,
                2:0,
                3:0,
                4:0
            }
    };
    
    function sendMessage() {
        clearTimeout(UIimg_interval);
        switches = 0;
        var message = {};
        message.sw = {};
        message.pass = $('meta[name=csrf-token]').attr('content');
        for (var i = 0; i < 4; i++) {
            if ($("input[id='sw" + i + "']:checked").length) {
                message.sw[i] = 1;
                switches = switches | 1 << i;

                labData["https://example.com/relle/acEletricPanel/actuator/switch"][i+1] = 1;
                console.log('switches: ' + switches);
            } else {
                message.sw[i] = 0;
                labData["https://example.com/relle/acEletricPanel/actuator/switch"][i+1] = 0;
            }
        }
        if (message && socket) {
            message.pass = $('meta[name=csrf-token]').attr('content');
            socket.emit('new message', message);
        }
        UIimg_interval = setTimeout(function () {
            if (circuit_images.indexOf(switches) >= 0) {
                console.log('loading /exp_data/2/1/equivalent/' + switches + '.png');
                $('.equivalent').attr('src', '/exp_data/2/1/equivalent/' + switches + '.png');
                $('.default_circuit').hide().addClass('hiddencircuit');
                $('.equivalent').show().removeClass('hiddencircuit');
            } else {
                $('.equivalent').hide().addClass('hiddencircuit');
                $('.default_circuit').show().removeClass('hiddencircuit');
                console.log('resultante ' + switches + ' nao criada ainda');
            }
        }, 3500);
    }

    $('.switch').change(function () {
        sendMessage();
        xapiCreateStatement({verb:'set', obj:{id:"https://example.com/relle/acEletricPanel/actuator/switch", name:xapiObjName(), type:'actuator'}, context:labData});
    });


    $.getScript(rpi_server + '/socket.io/socket.io.js', function () {
        // Initialize varibles
        // Prompt for setting a username
        socket = io.connect(rpi_server);
        socket.emit('new connection', {pass: $('meta[name=csrf-token]').attr('content')});
        
        xapiCreateStatement({verb:'initialized', obj:{id:window.location.href, name:xapiObjName(), type:'laboratory'}, context:labData});
        
        socket.on('reconnect', function () {
            socket.emit('new connection', {pass: $('meta[name=csrf-token]').attr('content')} );
        });

        socket.on('new message', function (data) {
            console.log(data);
        });

        $(".controllers").show();
        $(".loading").hide();

        // Whenever the server emits 'user joined', log it in the chat body
        socket.on('data received', function (data) {
            //printLog(data);
            results = $.parseJSON(data);
            console.log("I'm receiving " + data);

            for (var i = 0; i < 7; i++) {
                //console.log(results.amperemeter[i]);
                $("#a" + i).html(results.amperemeter[i] / 1000 + " mA");
            }
            for (var i = 0; i < 2; i++) {
                //console.log(results.voltmeter[i]);
                $("#v" + i).html(results.voltmeter[i] / 1000 + " V");
            }

        });
        // Limpar
        $('#btnLeave').on('click', function () {
            if (socket) {
                socket.disconnect();
                socket = null;
            }
        });


    });

});

function report(id) {
    var array = results; //$.parseJSON(results);   //JSON formatado como variável results no topo
    $.ajax({
        type: "POST",
        url: "http://relle.ufsc.br/labs/" + id + "/report/",
        data: array, // results, //{a2: 'i'}, // results,
        success: function (pdf) {
            // window.open("http://relle.ufsc.br/lara/labs/" + id + "/report", "_TOP");
            //safari.self.browserWindow
            //window.focus();
            var win = window.open("http://relle.ufsc.br/labs/" + id + "/report", '_blank');
            win.focus();
            console.log("Report created.");
        }
    });
}


