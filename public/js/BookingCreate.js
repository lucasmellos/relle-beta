    $.ajaxSetup({
        headers: { 'X-CSRF-Token' : $('meta[name=csrf-token]').attr('content') }
    });
    
    var lab_id, time, date, duration, durationMin;
    
    $(document).ready(function () {
        $('#datepickerID').datepicker({
            format: "yyyy-mm-dd",
            language: "pt-br"
        });
    });
      
    var e = document.getElementById("duration2");
    var itemSelected = e.options[e.selectedIndex].value;
        
    $(document).ready(function(){
        var previousvalue = itemSelected;
        $('#duration2').on('change',function(){
            if(previousvalue === "nmin" && $(this).val() === "nse") {
                document.getElementById("durationBox").value = "1";
                itemSelected = "nse";
            }
            else if(previousvalue === "nse" && $(this).val() === "nmin"){
                document.getElementById("durationBox").value = "10";
                itemSelected = "nmin";
            }
            previousvalue = $(this).val();
        });
    });
    
    function calcDuration() {
        var user_durac = $("#durationBox").val();
        var exp_durac = $('#exp_id option:selected').data("duration");
        if (itemSelected === "nse") {
            durationMin = user_durac * exp_durac;
        } else {
            durationMin = user_durac;
        }
    };
    
    function submitOutput() {
        date = document.getElementById('datepickerID').value;
        time = document.getElementById('timepickerID').value;
        lab_id = $('#exp_id option:selected').val();
        duration = durationMin;
        
        $.ajax({
        method: "POST",
        url: "",
        data: { date: date, time: time, lab_id: lab_id, duration: duration }
        })
        .done(function(data) {
            if(typeof(data)=="object"){
                if(data.erro){
                    if(data.erro == 1 ){
                        $("#error1").show(1000);
                        return;
                    }                   
                    if(data.erro == 2){
                        $("#error2").show(1000);
                        return;
                    }
                    if(data.erro == 3){
                        $("#error3").show(1000);
                        return;
                    }   
                }
                window.location.replace("/booking/all");
            } else{
                if(duration == "" || time == "" || date == "")
                    $("#error5").show(1000);
                else
                    $("#error4").show(1000); 
        }
    });
}
    function mudarDescricao(){      
        varDuration = $('#exp_id option:selected').data("duration");
        $('#small1').text(varDuration)                   
    }