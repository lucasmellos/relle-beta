$(function(){
    lrs=null;

    try {
        lrs = new TinCan.LRS(
                {
                    endpoint: "http://150.162.233.194/data/xAPI",
                    username: "19227b1e3d7624368ffb09457a1575b6ffce737e",
                    password: "d0a241de544d016c79c24c294f476cf23fb8f3ba",
                    allowFail: false
                }
        );
        console.log("Connected to LRS");
        
        
    } catch (ex) {
        console.log("Failed to setup LRS object: " + ex);
        // TODO: do something with error, can't communicate with LRS
    }
    
   xapiCreateStatement({verb:'launched', obj:{id:window.location.href, name:xapiObjName(), type:'laboratory'}});

    
});


function xapiCreateStatement(data) {
    var obj;
    var stmt;
    
    var verb = xapiVerb()[data.verb];
    
    if('actor' in data){
        //lab as an actor
        console.log('lab actor');
        obj = {
            "actor": data.actor,
            "verb": {id: verb, display:formatLocale(data.verb)},
            "object": {
                objectType:'Agent',
                mbox: xapiActor()['mbox'],
                name: xapiActor()['name']
            }
        };

    }else{
        //user as an actor
        console.log('user actor');
        var object_data = ('definition' in data.obj) ? data.obj : xapiObject(data.obj.id, data.obj.name, xapiObjTipe()[data.obj.type]);
        object_data.objectType = "Activity";
        obj = {
            "actor": xapiActor(),
            "verb": {id: verb, display:formatLocale(data.verb)},
            "object": object_data
        };
    }

    if (("context" in data)) {
        obj["context"] = xapiContext(data.context);
    }
    
    
    if (("result" in data)) {
        //obj["result"] = ('success' in data.result) ? data.result : getResult(data.result);
    }
    stmt = new TinCan.Statement(obj);
    
    lrs.saveStatement(stmt,
            {
                callback: function (err, xhr) {
                    if (err !== null) {
                        if (xhr !== null) {
                            console.log("Failed to save statement: " + xhr.responseText + " (" + xhr.status + ")");
                            // TODO: do something with error, didn't save statement
                            return;
                        }
                        console.log("Failed to save statement: " + err);
                        // TODO: do something with error, didn't save statement
                        return;
                    }
                    console.log("Statement saved");
                    // TOOO: do something with success (possibly ignore)
                }
            }
    );
}

function xapiActor() {
    return {
        "name": $('#user-data').data('name'),
        "mbox": $('#user-data').data('email')
    };
}

function xapiObject(id, name, type) {
    return {
        "id": id,
        "objectType": "Activity",
        "definition": {
            "name": name,
            "type": type
        }
    };
}

function xapiObjName(){
    var title = $('title').text().split("|")[0];
    return formatLocale(title);
}



function formatLocale(value){
    var lang = $('#lang-data').data('name');
    var obj = {};
    obj[lang] = value;
    return obj;
}

function xapiObjTipe(){
    return {
            "laboratory": "http://example.com/relle/onlineLabs/laboratory",
            "sensor": "http://example.com/relle/onlineLabs/sensor",
            "actuator": "http://example.com/relle/onlineLabs/actuator",
            "tutorial": "http://example.com/relle/onlineLabs/tutorial",
            "question" : "http://adlnet.gov/expapi/activities/question",
    };
}

function xapiVerb(){
    return {
        launched:"http://adlnet.gov/expapi/verbs/launched",
        initialized:"http://adlnet.gov/expapi/verbs/initialized",
        exited: "http://adlnet.gov/expapi/verbs/exited",
        abandoned: "http://adlnet.gov/expapi/verbs/abandoned",
        set: "https://example.com/relle/verbs/set",
        answered: "http://adlnet.gov/expapi/verbs/answered",
        completed: 'https://w3id.org/xapi/cmi5#completed',
        attempted: 'http://adlnet.gov/expapi/verbs/attempted',
        moved: "https://example.com/relle/verbs/moved",
        viewed: "http://id.tincanapi.com/verb/viewed",
        returned: "http://activitystrea.ms/schema/1.0/return", 
    };
}

function xapiContext(data){
    var context = {"extensions":{}};

    context["extensions"]['https://example.com/relle/lab-information'] = data;
    //console.log(context);
    return context;
    }