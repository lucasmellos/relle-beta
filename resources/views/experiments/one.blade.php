{{Analytics::trackEvent('Página', 'Experiments')}}

@extends ('layout.default')

@section('page')
{{trans('interface.name', ['page'=>$exp['name_'.App::getLocale()]])}}
@stop 

@section('head')
<link href="{{ asset('/css/one.css') }}" rel="stylesheet">
<link href="http://relle.ufsc.br/teste/css/botao.css" rel="stylesheet">
<style>
    #access{
        padding: 7px 19px;
        font-size: 17px;
        line-height: 1.471;
        border-radius: 6px;
        display: block;
        width: 100%;
    }
    .tile>p{
        text-align: left;
        font-size: 13pt;
        margin-bottom:7px;
        line-height: 1.5;
    }   
    .select2-choice{
        height:30px;
        width: auto;
        padding-top:3px;
        padding-bottom:3px;
    }
    .select2-container{
        width: auto !important;
        min-width:0;
    }
    #progress{
        background-color: #1abc9c;
    }
    @media only screen and (max-width: 768px) {
    /* For mobile phones: */
    #access {
        margin-top:-10px;
        background:#1abc9c;
    }
    
    .tile{
        height:auto;
    }
}
</style>
@stop
<!-- Facebook Comments  -->
<script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6&appId=1771373809748322";
            fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<div id="fb-root"></div>
<?php
$files = 'exp_data/' . $exp['id'] . '/';
$name='name_'.App::getLocale();
$desc='description_'.App::getLocale();
$tutorial = 'tutorial_'.App::getLocale();
?>

@section ('content')

<div id='identifier'></div>
<div id='close'></div>
<div id='return'></div>
<div id='error'></div>

<div id='pre_experiment'>
    <?php
    $browser = get_browser($_SERVER['HTTP_USER_AGENT'], true)['browser'];
    $mobile = get_browser($_SERVER['HTTP_USER_AGENT'], true)['ismobiledevice'];    
    ?>
    <div class="row">
        <div class="col-md-8 col-sm-12 tile">
            <h4>{{$exp[$name]}}</h4>
            <p style="text-align:left;">
                <strong>{{trans('labs.one')}}: </strong>
                <a href="{{url('labs/'.$exp->lab_id.'/exp/'.$exp->id)}}">{{$exp->lab->$name}}</a>
            </p>  
            <p style="text-align:left;">
                <strong>{{trans('labs.description')}}: </strong>
                {{$exp[$desc]}}
            </p>  
            <p style="text-align:left;">
                <strong>Tags: </strong> <span id="tags-lab"></span>
            </p>
            <br>
            <div id="main" class="col-md-6 col-md-offset-3 col-xs-6 col-xs-offset-3">
                <button id="access" class="btn btn-primary btn-large btn-block"><spam id="acessar">{{trans('interface.access')}}</spam></button>
           </div> 
            <br>

        </div>
        <!-- END OF LEFT -->

        <div class="col-md-4 col-sm-12 right-menu">
            <a href="https://goo.gl/forms/SCBbvf2CFBRGzyYf1" target="_blank" class="btn btn-block btn-lg btn-default" style="width: 90%; margin-bottom:25px;">Avalie este Experimento</a>

            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#tab_tutorial" aria-expanded="true" aria-controls="collapseOne">
                    <div class="panel">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h4 class="panel-title">
                                <i class="fa fa-info" aria-hidden="true" style="padding-right: 10px"></i>  Tutorial
                            </h4>
                        </div>
                    </div>
                </a>
                <div id="tab_tutorial" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body  tab-body">
                        <?php echo $exp->lab->$tutorial; ?>
                    </div>
                </div>
            </div>

            
        </div>

    </div>
</div>
<h3 id='title-inside' style="text-align:center" hidden>{{$exp[$name]}}</h3>
<div class="row">
    <div class="col-lg-6">
        <center><div id='exp'></div></center>
    </div>
    <div class="col-lg-6">
        <div class="progress" hidden>
            <div class="progress-bar progress-bar-success" id='progress' role="progressbar" aria-valuenow="0"
            aria-valuemin="0" aria-valuemax="100" style="width:0%">
            </div>
        </div>
    
        <div id='question-list'></div>
        
    </div>
</div>
<div id="leave"></div> 
@foreach($inst as $one)
<span hidden class="instances" id="instance-{{$one->id}}" data-address="{{$one->address}}"></span>
@endforeach

@stop   

@section('script')
<script src="{{ asset('js/xapi-client.js') }}"></script>
<script src="{{ asset('js/tether.js') }}"></script>
<script src="{{ asset('js/shepherd.js') }}"></script>
<script src="{{ asset('/js/jquery.redirect.js') }}"></script>

<script>
$(function(){
    //global variables
    /*
    var data_instances = [];
    @foreach($inst as $one)
        data_instances['{{$one->id}}'] = '{{$one->address}}';
    @endforeach
    console.log(data_instances);
    var data_selected;
    */
   
});
</script>

<script>
exp_name = "{!!$exp['name_pt']!!}";
str = "{{$exp['tags']}}";
questions = {!!$questions!!};
tags = str.split(", ");
progress = 0;
chunk = 100/Object.keys(questions).length;

function nextQuestion(id){
        
    progress+=chunk;
    $('#'+id).hide();
    
    $('#progress').css('width', progress+'%');
    
    if($('#'+id).is(':last-child')){
        $('#question-list').append('<h5>Experimento concluído com sucesso</h5>');
        $('#question-list').append('<button class="btn btn-sm btn-default" onclick="leaveExp()">Sair</button>');
        $('#'+id).remove();
    }else{
        
        $('#'+id).next().show();
        $('#'+id).remove();
    } 
};

    function leaveExp(){
        sendStatement({actor:getActorUser(), verb:'completed', obj:{id:window.location.href, name:{'pt-BR':$('#title-inside').html()}, id:window.location.href}, "result":{"success":true}, context: labData});
        location.reload();
    }

$(function(){
    
    

    for (var i = 0; i < tags.length; i++) {
        $('#tags-lab').append("<a href='#' class='tag-lab' data-tag='" + tags[i] + "' >" + tags[i] + "</a>")
    }   

    $("#link").click(function () {
        $("#embeed").show().select();
    });
    $('.tag-lab').click(function(){
        $.redirect("{{url('search')}}", { terms: $(this).attr('data-tag')});
    });
    $('#embeed').select();

    $.ajaxSetup({
        headers: { 'X-CSRF-Token' : $('meta[name=csrf-token]').attr('content') }
    });


    //User data to xAPI
    $('body').prepend("<span id='user-data' data-name='<?php echo Auth::user()->name;?>' data-email='<?php echo Auth::user()->email;?>'></span>");
    //$('body').prepend("<span id='lab-data' data-id='<?php echo $exp->lab->id; ?>'></span>");
   

});

</script>
<?php if (!$exp->lab->queue) { ?>
    <script>
        $(function(){

        var url = '{{asset("/exp_data/".$exp['lab_id']."/exp/".$exp['id'])}}'; console.log('URL: '+url);
        var html = url + "/{{App::getLocale()}}.html";
        var js = url + '/exp_script.js';
        var css = url + '/exp_style.css';
        $('#access').click(function(){
            $('#pre_experiment').remove();
            $('#title-inside').html("<h3>{{$exp[$name]}}</h3>");
            $('head').append('<link rel="stylesheet" href="' + css + '" type="text/css" />');
            $('#exp').load(html);
            $.getScript(js);
        });

        /*
        $('#view').click(function(){
            $('#pre_experiment').remove();
            $('head').append('<link rel="stylesheet" href="' + css + '" type="text/css" />');
            $('#exp').load(html);
            $('#title-inside').html("<h3>{{$exp[$name]}}</h3>");
            $.getScript(js);
        });
        */
        });
</script>
<?php } else{ ?>

        <script>

        var url = '{{asset("/exp_data/".$exp['lab_id']."/exp/".$exp['id'])}}';

        var html = url + "/{{App::getLocale()}}.html";
        
        //js xAPI instead of normal js
        var js = url + '/exp_script.js';
        var css = url + '/exp_style.css';
        var urlqueue = 'http://' + window.location.hostname + '/api/';
        var duration = {{$exp['lab']->duration}};
        var exp_id = {{$exp['lab_id']}};
        var locale = "{{App::getLocale()}}";
        var uilang = {};
        uilang.close_message = "<p> {{trans('interface.closelab')}}</p> <p>{{trans('interface.redirect')}}</p><br>";
        uilang.end = "{{trans('labs.end')}}";
        uilang.leave = "{{trans('interface.leave')}}";
        uilang.end_rep = "{{trans('labs.end_rep')}}";
        uilang.report = "{{trans('labs.report')}}";
        uilang.csvreport = "{{trans('labs.csvexport')}}";
        uilang.attention = "{{trans('interface.atention')}}";
        uilang.popup = "{{trans('interface.popup')}}"; 
        uilang.wait = "{{trans('interface.wait')}}";
        uilang.message_error = "{{trans('message.tab')}}";
        uilang.message_error_token = "{{trans('message.token')}}";
        uilang.timeleft = "{{trans('interface.timeleft')}}";
        uilang.reconnectingheader = "{{trans('interface.reconnecting_header')}}"; 
        uilang.reconnectingbody = "{{trans('interface.reconnecting_body')}}";
        uilang.labsunavailable = "{{trans('interface.labs_unavailable')}}"; 
        uilang.resources = "{{trans('interface.resources')}}";
        uilang.notice = "{{trans('message.notice')}}"
    </script>

    <script src="{{ asset('js/queue_design.js') }}"></script> 
    <!--<script src="{{ asset('js/queue.js') }}"></script>-->
    
    <!-- TEMPORÁRIO
    <script src="{{ asset('js/queue_exp_gambiarra.js') }}"></script>
    -->
 <?php } ?>
    
    <script src="{{ asset('js/socket.io.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/i18next/3.4.3/i18next.min.js" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-i18next/1.1.0/jquery-i18next.min.js" ></script>
@stop
